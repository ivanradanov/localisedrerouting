#!/bin/bash

set -x

PROCESS_NUM="$1"

for N in $(seq 0 $(expr $PROCESS_NUM - 1)); do
    numactl --cpunodebind=$N --membind=$N -- python3 -m scripts.test_nue --global_num_processes $PROCESS_NUM --num_processes 1 --seed 1 --process_id $N &
done
