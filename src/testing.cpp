#include <iostream>
#include <boost/random.hpp>

#include "graph.hpp"

void test_failure(Failure f, BoostGraph g, std::ostream &out = std::cout)
{
	auto ap = graph_get_affected_partitions(g, f);

	/* TODO do we need edge number in partitions?
	   might be slow to compute */
	out << "{ \"num_partitions\": " << ap.size()
	    << ", \"num_nodes\": " << graph_get_partitions_node_num(g, ap)
	    << ", \"affected_partitions\": [ ";
	for (auto p : ap)
		out << p << ", ";
	out << "]}," << std::endl;
}

BoostGraph test_init(int nparts, std::istream &in)
{
	BoostGraph g = load_graphviz(in);

	PartOps ops = {0};
	ops.nparts = nparts;

	partition_boost_graph(g, &ops);

	return g;
}

void test()
{
	BoostGraph g = test_init(10, std::cin);

	write_graphviz(g, std::cout);

	boost::random::mt19937 gen;

	auto f = generate_random_failure(g, gen, 1, 0);
	std::cerr << "vertex fail" << std::endl;
	for (auto f : f.vs)
		std::cerr << f << std::endl;
	test_failure(f, g, std::cerr);

	f = generate_random_failure(g, gen, 0, 1);
	std::cerr << "edge fail" << std::endl;
	for (auto f : f.es)
		std::cerr << f << std::endl;
	test_failure(f, g, std::cerr);

}

int main (int argc, char **argv)
{
	test();
	return 0;
}
