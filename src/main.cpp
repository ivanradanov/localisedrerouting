#include <iostream>
#include <boost/random.hpp>
#include <boost/program_options.hpp>

#include "graph.hpp"

BoostGraph test_init(int nparts, std::istream &in, bool ignore_terminal, bool include_terminal)
{
	BoostGraph og = load_graphviz(in);
	BoostGraph g = og;

	if (ignore_terminal)
		remove_terminal_nodes(g);

	PartOps ops = {0};
	ops.nparts = nparts;

	partition_boost_graph(g, &ops);

	if (ignore_terminal && include_terminal) {
		transfer_partition_ids(g, og);
		return og;
	} else {
		return g;
	}

}

void test_failure(Failure f, BoostGraph g, std::ostream &out = std::cout)
{
	auto ap = graph_get_affected_partitions(g, f);

	/* TODO do we need edge number in partitions?
	   might be slow to compute */
	out << "{ \"num_partitions\": " << ap.size()
		<< ", \"num_nodes\": " << graph_get_partitions_node_num(g, ap)
		<< ", \"affected_partitions\": [ ";
	for (auto p : ap)
		out << p << ", ";
	out << "]}," << std::endl;
}

typedef boost::random::mt19937 RandomGenerator;

void test_failures(int nfails, int nv, int ne, BoostGraph &g,
				   RandomGenerator &gen, std::ostream &out = std::cout)
{
	out << "\"tests\": [" << std::endl;
	for (int i = 0; i < nfails; i++) {
		auto f = generate_random_failure(g, gen, nv, ne);
		test_failure(f, g, out);
	}
	out << "]" << std::endl;
}

void write_graph_info(BoostGraph &g, std::ostream &out)
{
	out << "\"graph_info\": {" << std::endl
		<< "\"node_num\": " << boost::num_vertices(g) << "," << std::endl
		<< "\"link_num\": " << boost::num_edges(g) << "," << std::endl
		<< "\"partition_num\": " << g[boost::graph_bundle].partition_sizes.size() << "," << std::endl
		<< "\"partition_sizes\": {" << std::endl;
	for (int i = 0; i < g[boost::graph_bundle].partition_sizes.size(); i++)
		out << "\"" << i << "\":" << g[boost::graph_bundle].partition_sizes[i] << "," << std::endl;
	out << "}" << std::endl << "}" << std::endl;
}

void test_random_failures(int nparts, int ntests, int nv, int ne,
						  std::istream &in, std::ostream &out = std::cout,
                          bool ignore_terminal = false, bool include_terminal = false)
{
	BoostGraph g = test_init(nparts, in, ignore_terminal, include_terminal);

	boost::random::mt19937 gen;

	out << "{" << std::endl;

	write_graph_info(g, out);

	out << "," << std::endl;

	test_failures(ntests, nv, ne, g, gen, std::cout);

	out << "}" << std::endl;
}

void output_partitioned_graph(int nparts, std::istream &in, std::ostream &out = std::cout,
                              bool ignore_terminal = false, bool include_terminal = false)
{
	BoostGraph g = test_init(nparts, in, ignore_terminal, include_terminal);
	write_graphviz(g, out);
}

int main(int argc, char **argv)
{
	namespace po = boost::program_options;
	std::string in_file;
	std::string out_file;
	po::options_description desc("Options");
	desc.add_options()
		("nparts", po::value<int>(), "number of parts to partition graph in")
		("ntests", po::value<int>(), "number of tests to perform")
		("nv", po::value<int>(), "number of vertex(node) failures")
		("ne", po::value<int>(), "number of link(edge) failures")
		("in-file", po::value<std::string>(&in_file)->default_value("-"),  "graph file, - for stdin")
		("out-file", po::value<std::string>(&out_file)->default_value("-"),  "output")
		("only-partition", po::bool_switch()->default_value(false), "only partition")
		("ignore-terminal", po::bool_switch()->default_value(false), "ignore terminal nodes when partitioning")
		("include-terminal", po::bool_switch()->default_value(false), "include the terminal nodes in the resulting graph")
		;
	po::positional_options_description p;
	p.add("file", -1);
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv)
			  .options(desc)
			  .positional(p)
			  .run(), vm);
	po::notify(vm);

	std::istream *in;
	std::ifstream ifile;
	if (vm["in-file"].as<std::string>() == "-") {
		in = &std::cin;
	} else {
		ifile.open(vm["in-file"].as<std::string>());
		in = &ifile;
	}

	std::ostream *out;
	std::ofstream ofile;
	if (vm["out-file"].as<std::string>() == "-") {
		out = &std::cout;
	} else {
		ofile.open(vm["out-file"].as<std::string>());
		out = &ofile;
	}

	if (vm["only-partition"].as<bool>()) {
		output_partitioned_graph(
			vm["nparts"].as<int>(),
			*in, *out,
			vm["ignore-terminal"].as<bool>(),
			vm["include-terminal"].as<bool>());
	} else {
		test_random_failures(
			vm["nparts"].as<int>(),
			vm["ntests"].as<int>(),
			vm["nv"].as<int>(),
			vm["ne"].as<int>(),
			*in, *out,
			vm["ignore-terminal"].as<bool>(),
			vm["include-terminal"].as<bool>());
	}

	if (ifile.is_open())
		ifile.close();
	if (ofile.is_open())
		ofile.close();

	return 0;
}
