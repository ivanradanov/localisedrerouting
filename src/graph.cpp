#include "graph.hpp"

#include <map>
#include <cassert>

struct PropertyWriterV {
	BoostGraph &g;

	PropertyWriterV(BoostGraph &g) :g(g) {}

	void operator()(std::ostream& out, Vertex v) const {
		out <<
			"[" <<
			"partition=" << g[v].partition << " " <<
			"label=" << g[v].partition << " " <<
			"terminal=" << g[v].terminal << " " <<
			"]";
	}
};

struct PropertyWriterE {
	BoostGraph &g;

	PropertyWriterE(BoostGraph &g) :g(g) {}

	void operator()(std::ostream& out, Edge v) const {
	}
};

struct PropertyWriterG {
	BoostGraph &g;

	PropertyWriterG(BoostGraph &g) :g(g) {}

	void operator()(std::ostream& out) const {
	}
};

void write_graphviz(BoostGraph &g, std::ostream &stream)
{
	PropertyWriterV prop_writer_v(g);
	PropertyWriterE prop_writer_e(g);
	PropertyWriterG prop_writer_g(g);
	auto vid = boost::get(&Node::name, g);
	boost::write_graphviz(stream, g, prop_writer_v, prop_writer_e, prop_writer_g, vid);
}

BoostGraph load_graphviz(std::istream &stream)
{
	BoostGraph graph(0);
	boost::dynamic_properties dp;

	boost::property_map<BoostGraph, std::string Node::*>::type name =
		get(&Node::name, graph);
	dp.property("node_id",name);

	boost::property_map<BoostGraph, std::string Node::*>::type v_comment =
		get(&Node::comment, graph);
	dp.property("comment", v_comment);

	boost::property_map<BoostGraph, int Node::*>::type v_radix =
		get(&Node::radix, graph);
	dp.property("radix", v_radix);

	boost::property_map<BoostGraph, std::string Link::*>::type e_comment =
		get(&Link::comment, graph);
	dp.property("comment", e_comment);

	bool status = boost::read_graphviz(stream, graph, dp, "node_id");

	assert(status);

	tag_terminal_nodes(graph);

	return graph;
}

MetisGraph *graph_boost_to_metis(BoostGraph &bg,
								 std::map<Vertex, idx_t> &descriptor_index_map,
								 std::map<idx_t, Vertex> &index_descriptor_map)
{
	using namespace boost;

	MetisGraph *mg = new MetisGraph;
	mg->nvtxs = num_vertices(bg);
	mg->ncon = num_edges(bg);
	mg->xadj = new idx_t[mg->nvtxs + 1];
	mg->adjncy = new idx_t[2 * mg->ncon];
	mg->vwgt = NULL;
	mg->vsize = NULL;
	mg->adjwgt = NULL;

	/* build vertex name to index maps */
	int cur_v = 0;
	graph_traits<BoostGraph>::vertex_iterator v, v_end;
	for (tie(v, v_end) = vertices(bg); v != v_end; ++v, ++cur_v) {
		descriptor_index_map[*v] = cur_v;
		index_descriptor_map[cur_v] = *v;
	}

	cur_v = 0;
	int cur_e = 0;
	for (tie(v, v_end) = vertices(bg); v != v_end; ++v, ++cur_v) {
		mg->xadj[cur_v] = cur_e;
		graph_traits<BoostGraph>::out_edge_iterator e, e_end;
		for (tie(e, e_end) = out_edges(*v, bg); e != e_end; ++e, ++cur_e) {
			mg->adjncy[cur_e] = descriptor_index_map[target(*e, bg)];
		}
	}
	mg->xadj[cur_v] = cur_e;

	assert(cur_v == mg->nvtxs);
	assert(cur_e == mg->ncon * 2);

	return mg;
}

void partition_boost_graph(BoostGraph &g, PartOps *ops)
{
	using namespace boost;

	std::map<Vertex, idx_t> descriptor_index_map;
	std::map<idx_t, Vertex> index_descriptor_map;
	MetisGraph *mg = graph_boost_to_metis(g, descriptor_index_map, index_descriptor_map);

	PartRes res = {0};
	res.part = new idx_t[mg->nvtxs];

	partition_metis_graph(mg, ops, &res);

	g[boost::graph_bundle].partition_sizes.resize(ops->nparts);
	for (int i = 0; i < ops->nparts; i++)
		g[boost::graph_bundle].partition_sizes[res.part[i]] = 0;

	for (int i = 0; i < mg->nvtxs; i++) {
		g[index_descriptor_map[i]].partition = res.part[i];
		g[boost::graph_bundle].partition_sizes[res.part[i]]++;
	}

	delete[] res.part;
}

void delete_metis_graph(MetisGraph *mg)
{
	delete[] mg->xadj;
	delete[] mg->adjncy;
	delete mg;
}

int partition_metis_graph(MetisGraph *g, PartOps *o, PartRes *r)
{
	return METIS_PartGraphRecursive(&g->nvtxs, &g->ncon, g->xadj, g->adjncy, g->vwgt, g->vsize, g->adjwgt,
							   &o->nparts, o->tpwgts, o->ubvec, o->options,
							   &r->objval, r->part);
}

std::vector<Edge> graph_get_adjacent_edges(BoostGraph &g, Vertex &v)
{
	using namespace boost;
	std::vector<Edge> es;
	graph_traits<BoostGraph>::out_edge_iterator e, e_end;
	for (tie(e, e_end) = out_edges(v, g); e != e_end; ++e)
		es.push_back(*e);
	return es;
}

std::set<Partition> graph_get_affected_partitions_e(BoostGraph &g, Edge &e)
{
	std::set<Partition> ps;
	ps.insert(g[boost::target(e, g)].partition);
	ps.insert(g[boost::source(e, g)].partition);
	return ps;
}

std::set<Partition> graph_get_affected_partitions_v(BoostGraph &g, Vertex &v)
{
	auto es = graph_get_adjacent_edges(g, v);
	std::set<Partition> ps;
	for (auto e : es) {
		auto a = graph_get_affected_partitions_e(g, e);
		ps.insert(a.begin(), a.end());
	}
	return ps;
}

std::set<Partition> graph_get_affected_partitions(BoostGraph &g, Failure &f)
{
	std::set<Partition> ps;
	for (auto e : f.es) {
		auto a = graph_get_affected_partitions_e(g, e);
		ps.insert(a.begin(), a.end());
	}
	for (auto v : f.vs) {
		auto a = graph_get_affected_partitions_v(g, v);
		ps.insert(a.begin(), a.end());
	}
	return ps;
}

int graph_get_partitions_node_num(BoostGraph &g, std::set<Partition> &ps)
{
	int num = 0;
	for (auto p : ps)
		num += g[boost::graph_bundle].partition_sizes[p];
	return num;
}

void print_metis_graph(MetisGraph *mg)
{
	std::cout << "nvtx: " << mg->nvtxs << std::endl;
	std::cout << "ncon: " << mg->ncon << std::endl;

	std::cout << "xadj: ";
	for (int i = 0; i < mg->nvtxs + 1; i++)
		std::cout << mg->xadj[i] << " ";
	std::cout << std::endl;

	std::cout << "adjncy: ";
	for (int i = 0; i < mg->ncon; i++)
		std::cout << mg->adjncy[i] << " ";
	std::cout << std::endl;
}

void tag_terminal_nodes(BoostGraph &g)
{
	using namespace boost;
	graph_traits<BoostGraph>::vertex_iterator vi, vi_end, next;
	tie(vi, vi_end) = vertices(g);
	for (next = vi; vi != vi_end; vi = next) {
		++next;
		if (g[*vi].name[0] == 'T')
			g[*vi].terminal = true;
		else
			g[*vi].terminal = false;
	}
}

void remove_terminal_nodes(BoostGraph &g)
{
	using namespace boost;
	graph_traits<BoostGraph>::vertex_iterator vi, vi_end, next;
	tie(vi, vi_end) = vertices(g);
	for (next = vi; vi != vi_end; vi = next) {
		++next;
		if (g[*vi].name[0] == 'T') {
			graph_traits<BoostGraph>::out_edge_iterator e, e_end;
			for (tie(e, e_end) = out_edges(*vi, g); e != e_end; ++e)
				remove_edge(*e, g);
			remove_vertex(*vi, g);
		}
	}
}

void transfer_partition_ids(BoostGraph &pg, BoostGraph &g)
{
	using namespace boost;
	graph_traits<BoostGraph>::vertex_iterator vi, vi_end, inext;
	tie(vi, vi_end) = vertices(g);
	for (inext = vi; vi != vi_end; vi = inext) {
		++inext;
		if (g[*vi].terminal) {
			g[*vi].partition = -1;
		} else {
			// terrible
			graph_traits<BoostGraph>::vertex_iterator vj, vj_end, jnext;
			tie(vj, vj_end) = vertices(pg);
			for (jnext = vj; vj != vj_end; vj = jnext) {
				++jnext;
				if (g[*vi].name == pg[*vj].name) {
					g[*vi].partition = pg[*vj].partition;
					break;
				}
			}
		}
	}
}

