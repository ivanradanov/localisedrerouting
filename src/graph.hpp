#pragma once

#include <iostream>
#include <set>
#include <metis.h>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/random.hpp>

typedef int Partition;

struct Node {
	std::string name;
	Partition partition;
	bool terminal;
	int index;
	std::string comment;
	int radix;
};

struct Link {
	std::string comment;
};

struct Graph {
	std::vector<int> partition_sizes;
};

typedef boost::adjacency_list < boost::vecS, boost::listS, boost::undirectedS, Node, Link, Graph > BoostGraph;

typedef boost::graph_traits<BoostGraph>::vertex_descriptor Vertex;
typedef boost::graph_traits<BoostGraph>::edge_descriptor Edge;

struct Failure {
	std::set<Edge> es;
	std::set<Vertex> vs;
};

struct PartOps {
	idx_t nparts;
	real_t *tpwgts, *ubvec;
	idx_t *options;
};

struct PartRes {
	idx_t objval;
	idx_t *part;
};

struct MetisGraph {
	idx_t nvtxs, ncon;
	idx_t *xadj, *adjncy, *vwgt, *vsize, *adjwgt;
};

int partition_metis_graph(MetisGraph *g, PartOps *o, PartRes *r);

void partition_boost_graph(BoostGraph &g, PartOps *ops);

void write_graphviz(BoostGraph &g, std::ostream &stream);

BoostGraph load_graphviz(std::istream &stream);

MetisGraph *graph_boost_to_metis(BoostGraph &bg,
                                 std::map<Vertex, idx_t> &name_index_map,
                                 std::map<idx_t, Vertex> &index_name_map);

void delete_metis_graph(MetisGraph *mg);

void print_metis_graph(MetisGraph *mg);

template <class RandomNumGen>
Failure generate_random_failure(BoostGraph &g, RandomNumGen &gen, int num_v, int num_e);

std::set<Partition> graph_get_affected_partitions(BoostGraph &g, Failure &f);

int graph_get_partitions_node_num(BoostGraph &g, std::set<Partition> &ps);

void remove_terminal_nodes(BoostGraph &g);

void tag_terminal_nodes(BoostGraph &g);

void transfer_partition_ids(BoostGraph &pg, BoostGraph &g);

// impls

template <class RandomNumGen>
Failure generate_random_failure(BoostGraph &g, RandomNumGen &gen, int num_v, int num_e)
{
	Failure f;

	for (int i = 0; i < num_v; ) {
		auto v = boost::random_vertex(g, gen);
		/* already contained */
		if (f.vs.find(v) != f.vs.end()) {
			continue;
		} else {
			f.vs.insert(v);
			i++;
		}
	}

	for (int i = 0; i < num_e; ) {
		auto e = boost::random_edge(g, gen);
		/* already contained */
		if (f.es.find(e) != f.es.end()) {
			continue;
		} else {
			f.es.insert(e);
			i++;
		}
	}

	return f;
}
