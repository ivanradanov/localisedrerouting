import unittest

import itertools
import sys
import copy
import networkx as nx
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))


from scripts import partition
from scripts import updn_routing
from scripts import fault_gen
from scripts import hier_rout
from scripts import routing_eval
from scripts import hier_nue
from scripts.util import ilog, vlog, dlog
from scripts.hier_nue import opp

test_topologies = [
    partition.partitioned_topology_nx(partition.example_topologies[1], 10),
    partition.partitioned_topology_nx(partition.example_topologies[5], 10),
    partition.partitioned_topology_nx(partition.example_topologies[4], 10),
]

test_topologies_nue = [partition.non_partitioned_topology_nx(s) for s in [
    #partition.non_partitioned_topology_nx(partition.example_topologies[5]),
    #partition.non_partitioned_topology_nx(partition.example_topologies[4]),
    #partition.non_partitioned_topology_nx(partition.example_topologies[1]),
    #'mesh --dim 10 10',
    partition.args_for_multi_terminal('torus --dim 3 3 4'),
    partition.args_for_multi_terminal('torus --dim 5 5 5'),
    #partition.args_for_multi_terminal('hyperx --hxL 3 --hxS 5 5 4 --hxK 4 4 4'),
    #partition.args_for_multi_terminal('k-ary-n --tk 18 --tn 2'),
    #partition.args_for_multi_terminal('tofu --dims 16'),
    #partition.args_for_multi_terminal('tofu --dims 32'),
    #partition.args_for_multi_terminal('hypercube --dim 8'),
    #partition.args_for_multi_terminal('cascade --cg 1'),
    #partition.args_for_multi_terminal('dragonfly --da 10'),
    #partition.args_for_multi_terminal('k-ary-n --tk 20 --tn 3'),
]]

test_topologies_nue = list(itertools.chain.from_iterable((
    'torus --dim ' + str(x) + ' ' + str(x) + ' ' + str(x + 1),
    'torus --dim ' + str(x) + ' ' + str(x + 1) + ' ' + str(x + 1),
    'torus --dim ' + str(x + 1) + ' ' + str(x + 1) + ' ' + str(x + 1),
) for x in range(3, 8)))[0:][:1]

test_topologies_nue = [partition.non_partitioned_topology_nx(partition.args_for_multi_terminal(t)) for t in test_topologies_nue]

test_routings = [
    updn_routing.updn_route
]

fault_test_num = 5
test_failures = {
    g: (fault_gen.generate_random_failures(g, {'nv': 1, 'ne': 0}, fault_test_num) +
        fault_gen.generate_random_failures(g, {'nv': 0, 'ne': 1}, fault_test_num))
    for g in test_topologies}

fault_test_num = 50
test_failures_nue = {
    g: (fault_gen.generate_random_failures(g, {'nv': 1, 'ne': 0}, fault_test_num, only_switches = True) +
        fault_gen.generate_random_failures(g, {'nv': 0, 'ne': 1}, fault_test_num, only_switches = True))
    for g in test_topologies_nue}

def route_sanity_check(tester, g, ends, route):
    if ends[0] == ends[1]:
        tester.assertEqual([], route)
        return
    # start and end match
    tester.assertTrue(ends[0] == route[0][0])
    tester.assertTrue(ends[-1] == route[-1][-1])
    for (v, u) in route:
        if not v in g.nodes:
            ilog(route, v, u)
            tester.fail()
        if not u in g.nodes:
            ilog(route, v, u)
            tester.fail()
    for (v, u) in route:
        tester.assertTrue(g.has_edge(v, u))
    # all edges connect to each other
    i = 0
    while i + 1 < len(route):
        tester.assertTrue(route[i][1] == route[i + 1][0])
        i += 1

def routes_sanity_check(tester, topology, routes):
    for ends, route in routes.items():
        if route == None:
            dlog('route {} {} impasse'.format(ends, route))
        else:
            dlog('route {} {}'.format(ends, route))
            route_sanity_check(tester, topology, ends, route)

def check_all_pairs_have_routes(tester, topology, routes):
    terminals = [node for node, data in topology.nodes(data = True) if data['terminal']]
    for u in terminals:
        for v in terminals:
            if u != v:
                tester.assertTrue(routes[u, v] != None)

def check_routes_use_only_used_edges(tester, cdg, routes):
    for ends, route in routes.items():
        if route == None:
            continue
        for c1, c2 in zip(route, route[1:]):
            tester.assertTrue(cdg.edges[opp(c2), opp(c1)]['state'] == 'used')

class TestUpDn(unittest.TestCase):
    def test_updn(self):
        for g in test_topologies:
            routes = updn_routing.updn_route(g)
            root = updn_routing.updn_choose_root(g)
            updn_graph = updn_routing.updn_get_dir_assigned_graph(g, root)
            for ends, route in routes.items():
                route_sanity_check(self, g, ends, route)
                # check for illegal turns
                last = 'up'
                for edge in route:
                    next_e = updn_graph.edges[edge]['dir']
                    if last == 'down':
                        self.assertEqual(next_e, 'down')
                    last = next_e
            self.assertTrue(routing_eval.routing_is_deadlock_free(routes))


class TestRoutingEval(unittest.TestCase):
    def test_cdg_cycles(self):
        routes = {
            (1,3): [(1,2),(2,3)],
            (2,4): [(2,3),(3,4)],
            (3,1): [(3,4),(4,1)],
            (4,2): [(4,1),(1,2)],
        }
        self.assertFalse(routing_eval.routing_is_deadlock_free(routes))

class TestHierRout(unittest.TestCase):
    def test_hierrout(self):
        for g in test_topologies:
            for routing_fun in test_routings:
                proutes = routing_fun(g)
                for failure in test_failures[g]:
                    print(failure)
                    view = hier_rout.remove_failed_subgraph(g, failure)
                    routes = hier_rout.reroute(g, proutes, failure, routing_fun)
                    for ends, route in routes.items():
                        route_sanity_check(self, view, ends, route)
                    if True:
                        if routing_eval.routing_is_deadlock_free(routes):
                            print('Rerouting is deadlock free')
                        else:
                            print('!!! Rerouting is _NOT_ deadlock free !!!')

    def test_subgraph_fun(self):
        for g in test_topologies:
            for failure in test_failures[g]:
                view = hier_rout.remove_failed_subgraph(g, failure)
                for v in failure['vs']:
                    self.assertFalse(v in view.nodes)
                for e in failure['es']:
                    self.assertFalse(e in view.edges)
                    # TODO will break with multigraphs
                    self.assertFalse(tuple(reversed(e)) in view.edges)


class TestHierNue(unittest.TestCase):
    def test_cycle_free_with(self):
        topology = nx.complete_graph(10)
        for k, v in topology.nodes(data=True):
            v['terminal'] = False
        network = nx.DiGraph(topology)
        cdg = hier_nue.complete_cdg(network)
        hier_nue.assign_initial_weights(network, cdg)
        # tag_escape_paths_spanning_tree(topology, cdg)
        hier_nue.cycle_detection_prep(cdg)
        xnodes = [(1,2), (2,3), (3,1), (3,4), (4,5)]
        xedges = [((1,2), (2,3)), ((2,3),(3,1)), ((3,1), (1,2)), ((3,4), (4,5)), ((2,3), (3,4))]
        self.assertTrue(hier_nue.is_cycle_free_with(cdg, ((1,2), (2,3))))
        #print([x for x in cdg.nodes(data=True) if x[0] in xnodes])
        #print([x for x in cdg.edges(data=True) if (x[0], x[1]) in xedges])
        self.assertTrue(hier_nue.is_cycle_free_with(cdg, ((2,3), (3,1))))
        #print([x for x in cdg.nodes(data=True) if x[0] in xnodes])
        #print([x for x in cdg.edges(data=True) if (x[0], x[1]) in xedges])
        self.assertFalse(hier_nue.is_cycle_free_with(cdg, ((3,1), (1,2))))
        #print([x for x in cdg.nodes(data=True) if x[0] in xnodes])
        #print([x for x in cdg.edges(data=True) if (x[0], x[1]) in xedges])
        self.assertTrue(hier_nue.is_cycle_free_with(cdg, ((3,4), (4,5))))
        #print([x for x in cdg.nodes(data=True) if x[0] in xnodes])
        #print([x for x in cdg.edges(data=True) if (x[0], x[1]) in xedges])
        self.assertTrue(hier_nue.is_cycle_free_with(cdg, ((2,3), (3,4))))
        #print([x for x in cdg.nodes(data=True) if x[0] in xnodes])
        #print([x for x in cdg.edges(data=True) if (x[0], x[1]) in xedges])

    def test_cycle_free(self):
        g = nx.DiGraph()
        g.add_edge(0, 1, state='used')
        g.add_edge(1, 2, state='used')
        g.add_edge(2, 3, state='used')
        g.add_edge(2, 4, state='used')
        g.add_edge(4, 0, state='unused')
        hier_nue.cycle_detection_prep(g)
        self.assertTrue(hier_nue.is_cycle_free(g))
        for i in range(5):
            self.assertTrue(hier_nue.my_is_cycle_free(g, i))
        g.edges[4, 0]['state'] = 'used'
        hier_nue.cycle_detection_prep(g)
        self.assertFalse(hier_nue.is_cycle_free(g))
        for i in range(5):
            print(i)
            if i != 3:
                self.assertFalse(hier_nue.my_is_cycle_free(g, i))

    def test_nue(self):
        for g in test_topologies_nue:
            topology, network, cdg, routes = hier_nue.nue_routing_single_layer(g, None)
            for ends, route in routes.items():
                route_sanity_check(self, g, ends, route)
            self.assertTrue(routing_eval.routing_is_deadlock_free(routes))
            self.assertTrue(hier_nue.get_routing_tables(routes, network) != None)


    def test_local_nue(self):
        for g in test_topologies_nue:
            topology, network, cdg, routes, tree = hier_nue.nue_routing_single_layer(g, None)
            for ends, route in routes.items():
                route_sanity_check(self, g, ends, route)
            self.assertTrue(routing_eval.routing_is_deadlock_free(routes))
            self.assertTrue(hier_nue.get_routing_tables(routes, network) != None)

            for failure in test_failures_nue[g]:
                ilog('summary for graph {}, failure {}: '.format(g.graph['gen_params'], failure))
                ln = hier_nue.LocalNue(topology, network, cdg, routes)
                ln.reroute(failure)

                for ends, route in ln.routes.items():
                    if route == None:
                        dlog('route {} {} impasse'.format(ends, route))
                    else:
                        dlog('route {} {}'.format(ends, route))
                        route_sanity_check(self, ln.topology, ends, route)
                ilog(('routed_successfully {}\nreached impasse {}').format(
                    ln.successful_routes, ln.impasse_routes))

    def test_local_nue_alt_st(self):
        for g in test_topologies_nue:
            topology, network, cdg, routes, tree = hier_nue.nue_routing_single_layer(g, None)
            for ends, route in routes.items():
                route_sanity_check(self, g, ends, route)
            self.assertTrue(routing_eval.routing_is_deadlock_free(routes))
            #self.assertTrue(hier_nue.get_routing_tables(routes, network) != None)

            for failure in test_failures_nue[g]:
                ilog('summary for graph {}, failure {}: '.format(g.graph['gen_params'], failure))
                ln = hier_nue.LocalNue(topology, network, cdg, routes, spanning_tree = tree)
                ln.reroute_alt_st_preprocess(failure)
                ilog('number of trees: {}, fixed tree: {}'.format(ln.eval_number_of_spanning_tree_components,
                                                                  ln.eval_fix_st_escape_paths_success))

    def test_local_nue_alt_st_v2(self):
        for g in test_topologies_nue:
            topology, network, cdg, routes, tree = hier_nue.nue_routing_single_layer(g, None)
            for ends, route in routes.items():
                route_sanity_check(self, g, ends, route)
            self.assertTrue(routing_eval.routing_is_deadlock_free(routes))
            self.assertTrue(hier_nue.get_routing_tables(routes, network) != None)

            for failure in test_failures_nue[g]:
                ilog('summary for graph {}, failure {}: '.format(g.graph['gen_params'], failure))
                ln = hier_nue.LocalNue(topology, network, cdg, routes, spanning_tree = tree, target_size = 30)
                ln.reroute_alt_st_v2_preprocess(failure)
                ilog('number of trees: {}, fixed tree: {}'.format(ln.eval_number_of_spanning_tree_components, ln.eval_fix_st_escape_paths_success))
                ln.reroute()
                for ends, route in ln.routes.items():
                    if route == None:
                        dlog('route {} {} impasse'.format(ends, route))
                    else:
                        dlog('route {} {}'.format(ends, route))
                        route_sanity_check(self, ln.topology, ends, route)
                ilog(('routed_successfully {}\nreached impasse {}').format(
                    ln.successful_routes, ln.impasse_routes))
                self.assertTrue(routing_eval.routing_is_deadlock_free(ln.routes))
                #self.assertTrue(hier_nue.get_routing_tables(ln.routes, ln.network) != None)

    def test_local_nue_alt_st_v3(self):
        for g in test_topologies_nue:
            topology, network, cdg, routes, tree = hier_nue.nue_routing_single_layer(g, None)
            for ends, route in routes.items():
                route_sanity_check(self, g, ends, route)
            self.assertTrue(routing_eval.routing_is_deadlock_free(routes))
            self.assertTrue(hier_nue.get_routing_tables(routes, network) != None)
            check_routes_use_only_used_edges(self, cdg, routes)

            for failure in test_failures_nue[g]:
                ilog('eval package for graph {}, failure {}: '.format(g.graph['gen_params'], failure))
                ln = hier_nue.LocalNue(topology, network, cdg, routes, spanning_tree = tree, target_size = 0,
                                       ra_expansion_type = 'topology')
                ln.reroute_alt_st_v3_preprocess(failure)
                ln.reroute()

                ilog(ln.get_eval_package())

                combined_routes = copy.copy(ln.routes)
                for ends in ln.new_routes:
                    if ln.new_routes[ends] == None:
                        combined_routes[ends] = ln.new_escape_routes[ends]
                    else:
                        combined_routes[ends] = ln.new_routes[ends]

                routes_sanity_check(self, ln.topology, ln.new_routes)
                routes_sanity_check(self, ln.topology, ln.new_escape_routes)
                check_routes_use_only_used_edges(self, ln.cdg, ln.new_routes)
                check_routes_use_only_used_edges(self, ln.cdg, ln.new_escape_routes)
                check_all_pairs_have_routes(self, ln.topology, combined_routes)
                check_all_pairs_have_routes(self, ln.topology, ln.routes)
                self.assertTrue(routing_eval.routing_is_deadlock_free(ln.routes))
                self.assertTrue(routing_eval.routing_is_deadlock_free(combined_routes))
                self.assertTrue(hier_nue.get_routing_tables(ln.new_routes, ln.network) != None)
                self.assertTrue(hier_nue.get_routing_tables(ln.new_escape_routes, ln.network) != None)

                ilog()


if __name__ == '__main__':
    t = TestHierNue()
    t.test_local_nue_alt_st_v3()
    #unittest.main()
