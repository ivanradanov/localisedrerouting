#!/usr/bin/env python3

import subprocess
import io
import ast
import statistics
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from multiprocessing import Pool

from TopologyGenerator.io.dot import _write_dot_topology_out as write_topology

from . import topo_gen
from . import partition
from . import hier_rout
from . import fault_gen
from . import routing_eval
from . import updn_routing
from .util import ilog

default_fault_test_num = 100
process_num = 7

def run_tests(test_fun, tests):
    return [test_fun(test) for test in tests]
    with Pool(process_num) as p:
        res = p.map(test_fun, tests)
    return res

####################
### Partitioning ###
####################

# Affected partitions test
def build_ap_experiment(topo, nparts_range):
    ex = {}
    ex['x'] = nparts_range
    ex['xlabel'] = 'nparts'
    ex['ylabel'] = 'node number'
    ex['tests'] = [
        {'topology': topo,
        'nparts': n,
        'fault_generator': {'nv': 1, 'ne': 0},
        }
        for n in ex['x']]
    ex['title'] = 'affected area size ' + topo
    return ex

example_ap_experiment = build_ap_experiment( 'hyperx --hxL 4 --hxS 3 3 3 3 --hxK 1 1 1 1', list(range(4, 30)))

def save_ap_experiment_plot(ex):
    plot_ap_experiment(ex)
    plt.savefig('./figs/' + ex['title'] + str(ex['x']))

def plot_ap_experiment(ex):
    an = run_tests(run_affected_nodes_test, ex['tests'])
    nn = run_tests(run_nodes_in_graph_test, ex['tests'])
    #nip = run_tests(run_nodes_in_partition_test, ex['tests'])

    fig, ax = plt.subplots()
    ax.plot(ex['x'], nn)
    ax.violinplot(an, showmeans=True, showmedians=False, positions = ex['x'])
    #ax.plot(ex['x'], nip)
    ax.set_title(ex['title'])
    ax.set_xlabel(ex['xlabel'])
    ax.set_ylabel(ex['ylabel'])
    ax.set_ylim(ymin=0)

def run_nodes_in_graph_test(test, n = None):
    g = partition.partitioned_topology_nx(test['topology'], test['nparts'], ignore_terminal = True)
    res = len(g.nodes())
    return res

def run_nodes_in_partition_test(test, n = None):
    g = partition.partitioned_topology_nx(test['topology'], test['nparts'], ignore_terminal = True)
    res = statistics.mean([len(x) for x in hier_rout.partitions(g).values()])
    return res

def run_affected_nodes_test(test, n = None):
    if n == None:
        n = default_fault_test_num

    g = partition.partitioned_topology_nx(test['topology'], test['nparts'], ignore_terminal = True)
    fs = fault_gen.generate_random_failures(g, test['fault_generator'], n)

    res = [len(hier_rout.get_nodes_in_affected_partitions(g, f)) for f in fs]

    return res

###############
### Routing ###
###############

def plot_routing_experiment(ex):
    ilog('original edge forwarding index')
    original_edge_index = run_tests(run_routing_edge_forwarding_index_test, ex['tests'])
    ilog('edge forwarding index after rerouting')
    edge_index = run_tests(run_routing_reroute_edge_forwarding_index_test, ex['tests'])
    #nip = run_tests(run_nodes_in_partition_test, ex['tests'])

    fig, ax = plt.subplots()
    ax.plot(ex['x'], original_edge_index)
    ax.violinplot(edge_index, showmeans=True, showmedians=False, positions = ex['x'])
    ax.set_title(ex['title'])
    ax.set_xlabel(ex['xlabel'])
    ax.set_ylabel(ex['ylabel'])
    ax.set_ylim(ymin=0)

def build_routing_experiment(topo, nparts_range):
    ex = {}
    ex['x'] = nparts_range
    ex['xlabel'] = 'nparts'
    ex['ylabel'] = 'node number'
    ex['tests'] = [
        {'topology': topo,
         'nparts': n,
         'fault_generator': {'nv': 1, 'ne': 0},
         'routing_function': updn_routing.updn_route,
        }
        for n in ex['x']]
    ex['title'] = 'updn reroute ' + topo
    return ex

def run_routing_edge_forwarding_index_test(test, n = None):
    g = partition.partitioned_topology_nx(test['topology'], test['nparts'], ignore_terminal = True)
    routing_fun = updn_routing.updn_route
    routes = routing_fun(g)
    res = routing_eval.edge_forwarding_index(routes)
    return res

def run_routing_reroute_edge_forwarding_index_test(test, n = None):
    if n == None:
        n = default_fault_test_num

    g = partition.partitioned_topology_nx(test['topology'], test['nparts'], ignore_terminal = True)
    fs = fault_gen.generate_random_failures(g, test['fault_generator'], n)


    routing_fun = updn_routing.updn_route
    proutes = routing_fun(g)

    res = [routing_eval.edge_forwarding_index(hier_rout.reroute(g, proutes, f, routing_fun)) for f in fs]

    return res

# old

def summary(l):
    d = {}
    d['max'] = max(l)
    d['min'] = min(l)
    d['avg'] = statistics.mean(l)
    return d

def test_topology(topology_args, hierrout_options):
    f = io.StringIO('')

    topology_args = topo_gen._init_gen(topology_args)
    topology, supplement_material = topo_gen._topo_generation(topology_args)
    write_topology(f, topology, args = topology_args, supplement_material = supplement_material)

    args = [hierrout_binary]
    for k, v in hierrout_options.items():
        args += ['--' + k, str(v)]

    out = subprocess.Popen(args, stdin = subprocess.PIPE, stdout = subprocess.PIPE, text = True)
    outs, errs = out.communicate(f.getvalue())

    return ast.literal_eval(outs)

def consolidate_test_topology_out(out):

    def consolidate(d, l):
        d['max'] = max(l)
        d['min'] = min(l)
        d['avg'] = statistics.mean(l)

    part_sizes = out['graph_info']['partition_sizes']
    out['graph_info']['partition_sizes_'] = {}
    consolidate(out['graph_info']['partition_sizes_'], list(part_sizes.values()))
    del out['graph_info']['partition_sizes']

    tests = out['tests']
    out['tests_'] = {'partitions': {}, 'nodes': {}}
    consolidate(out['tests_']['partitions'], [x['num_partitions'] for x in tests])
    consolidate(out['tests_']['nodes'], [x['num_nodes'] for x in tests])
    del out['tests']

    return out


def test_topology_consolidated(topology_args, hierrout_options):
    out = test_topology(topology_args, hierrout_options)
    return consolidate_test_topology_out(out)

if __name__ == '__main__':
    exit(0)
