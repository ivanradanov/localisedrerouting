#!/usr/bin/env python3

import networkx as nx
import random

def generate_random_failures(g, fg, n, **kwargs):
    return generate_random_failures_simple(g, fg['nv'], fg['ne'], n, **kwargs)

def generate_random_failures_simple(g, nv, ne, n, **kwargs):
    return [generate_random_failure(g, nv, ne, **kwargs) for _ in range(n)]

def generate_random_failure(g, nv, ne, **kwargs):
    if 'only_switches' in kwargs:
        only_switches = kwargs['only_switches']
    else:
        only_switches = False
    f = {
        'vs': random.sample([node for node in g.nodes()
                             if not only_switches or not g.nodes[node]['terminal']] , nv),
        'es': random.sample([edge for edge in g.edges()
                             if not only_switches or ((not g.nodes[edge[0]]['terminal']) and
                                                             (not g.nodes[edge[1]]['terminal']))], ne)
    }
    return f
