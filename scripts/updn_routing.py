import networkx as nx
import os
from diskcache import Cache
from . import util
from .util import dlog, ilog
from copy import deepcopy

def __pair_edge(e):
    return (e[1], e[0])

def pair_edge(e):
    ne = list(deepcopy(e))
    ne[0] = e[1]
    ne[1] = e[0]
    return tuple(ne)

def updn_choose_root(g):
    return list(g)[0]

def updn_assign_dirs(g, root):
    for edge in g.edges:
        g.edges[edge]['dir'] = 'unknown'
    for edge in nx.bfs_edges(g, root):
        g.edges[edge]['dir'] = 'down'
    for edge in g.edges:
        if g.edges[edge]['dir'] == 'down':
            g.edges[pair_edge(edge)]['dir'] = 'up'
    for edge in g.edges:
        if g.edges[edge]['dir'] == 'unknown':
            if edge[0] < edge[1]:
                g.edges[edge]['dir'] = 'down'
            else:
                g.edges[edge]['dir'] = 'up'

    for edge in g.edges:
        assert(g.edges[edge]['dir'] != 'unknown')

# graph, from, to
# bfs with the up down link usage limitation
def updn_bfs(g, f, t):
    queue = [f]
    visited = [f]
    visited_down = [f]

    prev = {}
    for node in g.nodes:
        prev[node] = {'up': None, 'down': None}
    prev[f]['up'] = 'some'

    dlog('bfs from {} to {}'.format(f, t))
    while True:
        v = queue.pop(0)
        dlog('visited {}'.format(v))
        if v == t:
            break
        for u in g.neighbors(v):
            dlog('found neighbour {}'.format(u))
            if u in visited:
                dlog('{} was already visited'.format(f, t, v))
                continue
            direction = g[v][u]['dir']
            if direction == 'up':
                dlog('direction is up')
                if prev[v]['up'] == None:
                    dlog('illegal turn')
                    continue
                visited.append(u)
                prev[u][direction] = v
                queue.append(u)
            elif u not in visited_down:
                dlog('direction is down and not already visited by down')
                visited_down.append(u)
                prev[u][direction] = v
                queue.append(u)
    else:
        assert(False and "Did not find a path, impossible")
    return prev

def updn_find_path(g, f, t, debug = False):
    # first go down, then up, if the previous down field is not None, it should
    # be the shorter path due to how updn_bfs works

    def _updn_find_path_down(prev, f, t):
        if f == t:
            return []
        prevdown = prev[t]['down']
        prevup = prev[t]['up']
        if prevdown != None:
            if debug:
                return _updn_find_path_down(prev, f, prevdown) + [(prevdown, t, 'd')]
            else:
                return _updn_find_path_down(prev, f, prevdown) + [(prevdown, t)]
        else:
            assert(prevup != None)
            if debug:
                return _updn_find_path_up(prev, f, prevup) + [(prevup, t, 'u')]
            else:
                return _updn_find_path_up(prev, f, prevup) + [(prevup, t)]


    def _updn_find_path_up(prev, f, t):
        if f == t:
            return []
        prevup = prev[t]['up']
        assert(prevup != None)
        if debug:
            return _updn_find_path_up(prev, f, prevup) + [(prevup, t, 'u')]
        else:
            return _updn_find_path_up(prev, f, prevup) + [(prevup, t)]

    prev = updn_bfs(g, f, t)
    dlog('prev {}'.format(prev))
    return _updn_find_path_down(prev, f, t)

def updn_get_routes(g):
    routes = {}
    for f in g.nodes:
        for t in g.nodes:
            dlog('from {} to {}'.format(f, t))
            if f != t:
                routes[f, t] = updn_find_path(g, f, t)
            else:
                routes[f, t] = []
            dlog('route {}'.format(routes[f,t]))
    return routes

# Takes a topology graph (undirected)
def updn_get_dir_assigned_graph(g, root):
    g = g.copy()
    # convert to a directed graph
    g = nx.DiGraph(g)
    updn_assign_dirs(g, root)
    return g


updn_route_cache = Cache(os.getcwd() + '/updn_routing_cache')

# Takes a topology graph (undirected)
def updn_route(g):

    ilog('updn routing graph {}'.format(g))

    cache_idx = nx.weisfeiler_lehman_graph_hash(g)
    if cache_idx in updn_route_cache:
        return updn_route_cache[cache_idx]

    root = updn_choose_root(g)
    g = updn_get_dir_assigned_graph(g, root)
    r = updn_get_routes(g)

    updn_route_cache[cache_idx] = r

    ilog('result: {}'.format(r))

    return r



