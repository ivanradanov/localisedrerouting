#!/usr/bin/env python3

import os
import time
import subprocess
import io
import ast
import statistics
import networkx as nx
import pygraphviz as pgv
from diskcache import Cache

from . import topo_gen

from .fault_gen import generate_random_failures

from TopologyGenerator.io.dot import _write_dot_topology_out as write_topology

topology_cache = Cache(os.getcwd() + '/topology_cache')

hierrout_binary = './build/bin/hierrout'

example_topologies = [
    '--ot dot torus --dim 4 4 4',
    '--ot dot hyperx --hxL 4 --hxS 3 3 3 3 --hxK 1 1 1 1',
    '--ot dot hyperx --hxL 4 --hxS 3 3 4 4 --hxK 1 1 1 1',
    '--ot dot tofu --dims 1 2 4',
    '--ot dot torus --dim 5 6 6',
    '--ot dot mesh --dim 10 10',
    ]

example_topologies_no_terminal = [
    '--nt 0 --ot dot torus --dim 4 4 4',
    '--nt 0 --ot dot hyperx --hxL 4 --hxS 3 3 3 3 --hxK 1 1 1 1',
    '--nt 0 --ot dot hyperx --hxL 4 --hxS 3 3 4 4 --hxK 1 1 1 1',
    '--nt 0 --ot dot tofu --dims 1 2 4',
    '--nt 0 --ot dot torus --dim 5 6 6',
    '--nt 0 --ot dot mesh --dim 10 10',
    ]

example_hierrout_options = {
    'nparts': 20,
    'ntests': 100,
    'ne': 0,
    'nv': 1,
    'in-file': '-'
}

def partitioned_topology_dot(topology_args_s, nparts, ignore_terminal=True, include_terminal=False):
    print('Partitioning {} in {} parts'.format(topology_args_s, nparts))
    cache_idx = (topology_args_s, nparts, ignore_terminal, include_terminal)
    if cache_idx in topology_cache:
        return topology_cache[cache_idx]

    t0 = time.time()

    topology_args = ['--ot', 'dot'] + topology_args_s.split()
    f = io.StringIO('')

    topology_args = topo_gen._init_gen(topology_args)
    topology, supplement_material = topo_gen._topo_generation(topology_args)
    write_topology(f, topology, args = topology_args, supplement_material = supplement_material)

    args = [hierrout_binary, '--only-partition', '--nparts', str(nparts)]
    if ignore_terminal:
        args += ['--ignore-terminal']
    if include_terminal:
        args += ['--include-terminal']

    print('Executing {}'.format(args))
    out = subprocess.Popen(args, stdin = subprocess.PIPE, stdout = subprocess.PIPE, text = True)
    outs, errs = out.communicate(f.getvalue())

    topology_cache[cache_idx] = outs

    t1 = time.time()

    print('Took {} seconds'.format(t1 - t0))
    return outs

def partitioned_topology_nx(*args, **kwargs):
    dot = partitioned_topology_dot(*args, **kwargs)
    agraph = pgv.AGraph(string=dot, thing=None)
    graph = nx.nx_agraph.from_agraph(agraph)
    g =  nx.Graph(graph)

    for k, v in g.nodes(data=True):
        v['terminal'] = bool(int(v['terminal']))
        v['partition'] = int(v['partition'])

    # TODO converting node identifiers to integers could improve performance,
    # inverstigate

    g.graph['gen_params'] = args[0]

    return g

def non_partitioned_topology_nx(topology_args_s):
    topology_args = ['--ot', 'dot'] + topology_args_s.split()
    f = io.StringIO('')

    topology_args = topo_gen._init_gen(topology_args)
    topology, supplement_material = topo_gen._topo_generation(topology_args)
    write_topology(f, topology, args = topology_args, supplement_material = supplement_material)

    dot = f.getvalue()

    agraph = pgv.AGraph(string=dot, thing=None)
    graph = nx.nx_agraph.from_agraph(agraph)
    g =  nx.Graph(graph)

    for k, v in g.nodes(data=True):
        if k[0] == 'T':
            v['terminal'] = True
        elif k[0] == 'S':
            v['terminal'] = False
        else:
            assert(False)

    # TODO converting node identifiers to integers could improve performance,
    # investigate

    # should be left off if debugging
    g = nx.convert_node_labels_to_integers(g)

    g.graph['gen_params'] = topology_args_s

    return g

def topology_leaf_switches(topology):
    return {list(topology.adj[node])[0] for node in topology.nodes() if topology.nodes[node]['terminal']}

def topology_leaf_switch_num(topology):
    return len({list(topology.adj[node])[0] for node in topology.nodes() if topology.nodes[node]['terminal']})

def topology_switch_num(topology):
    return len({node for node in topology.nodes() if not topology.nodes[node]['terminal']})

def topology_terminal_num(topology):
    return len({node for node in topology.nodes() if topology.nodes[node]['terminal']})

def args_for_multi_terminal(topology_args_s):
    if 'cascade' in topology_args_s or 'dragonfly' in topology_args_s:
        return topology_args_s
    topology = non_partitioned_topology_nx(topology_args_s)
    leaf_switch_num = topology_leaf_switch_num(topology)
    terminal_num = topology_terminal_num(topology)
    assert(leaf_switch_num == terminal_num)

    leaf_switch_cons = [len(topology.adj[s]) - 1 for s in topology_leaf_switches(topology)]
    mean_cons_leaf_swiches = round(statistics.mean(leaf_switch_cons))

    topology_args_s = '--nt ' + str(mean_cons_leaf_swiches * leaf_switch_num) + ' ' + topology_args_s

    return topology_args_s

if __name__ == '__main__':
    exit(0)
