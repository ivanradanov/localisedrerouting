#!/usr/bin/env python3

# Copyright (c) 2009-2018, Jens Domke
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from sys import exit, argv
from collections import namedtuple
from cProfile import Profile
from io import StringIO
from pstats import Stats
import TopologyGenerator as tpg


def _init_gen(local_argv):
    arg_parser = tpg.common._get_arg_parser()

    tpg.common._set_global_options(arg_parser)
    arg_parser.add_argument(
        '--profile',
        action='store_true',
        dest='profile',
        help='profile the execution of this script and print a summery',
        default=False)
    tpg.io.infiniband._set_local_options(arg_parser)
    tpg.io.dot._set_local_options(arg_parser)

    arg_subparsers = arg_parser.add_subparsers(
        title='Topologies',
        description='select one of the following',
        help='append --help to get topology-specific help pages',
        dest='topology')
    tpg.KaryN._set_local_options(arg_subparsers)
    tpg.XGFT._set_local_options(arg_subparsers)
    tpg.FatTree._set_local_options(arg_subparsers)
    tpg.Mesh._set_local_options(arg_subparsers)
    tpg.ExpressMesh._set_local_options(arg_subparsers)
    tpg.Torus._set_local_options(arg_subparsers)
    tpg.Ring._set_local_options(arg_subparsers)
    tpg.HyperCube._set_local_options(arg_subparsers)
    tpg.HyperX._set_local_options(arg_subparsers)
    tpg.Tofu._set_local_options(arg_subparsers)
    tpg.Arrangement._set_local_options(arg_subparsers)
    tpg.Kautz._set_local_options(arg_subparsers)
    tpg.CompleteDragonfly._set_local_options(arg_subparsers)
    tpg.Cascade._set_local_options(arg_subparsers)
    tpg.RandomTopo._set_local_options(arg_subparsers)
    tpg.LoadTopo._set_local_options(arg_subparsers)

    args = vars(arg_parser.parse_args(local_argv))
    args['__argparse_used__'] = 1
    tpg.common._init_rng(args)

    return args


def _topo_generation(args):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')
    topo_type = args.get('topology')
    if not topo_type:
        exit('ERR: no topology selected, see %s --help for details!' % argv[0])

    appendix = {}
    if topo_type == 'k-ary-n':
        topology = tpg.KaryN.generate_topology(args)
    elif topo_type == 'xgft':
        topology = tpg.XGFT.generate_topology(args)
    elif topo_type == 'fat-tree':
        topology = tpg.FatTree.generate_topology(args)
    elif topo_type == 'mesh':
        if args.get('mesh_extended_material'):
            topology, appendix = tpg.Mesh.generate_topology(args)
        else:
            topology = tpg.Mesh.generate_topology(args)
    elif topo_type == 'expmesh':
        topology = tpg.ExpressMesh.generate_topology(args)
    elif topo_type == 'torus':
        if args.get('torus_extended_material'):
            topology, appendix = tpg.Torus.generate_topology(args)
        else:
            topology = tpg.Torus.generate_topology(args)
    elif topo_type == 'ring':
        topology = tpg.Ring.generate_topology(args)
    elif topo_type == 'hypercube':
        topology = tpg.HyperCube.generate_topology(args)
    elif topo_type == 'hyperx':
        topology = tpg.HyperX.generate_topology(args)
    elif topo_type == 'tofu':
        if args.get('tofu_extended_material'):
            topology, appendix = tpg.Tofu.generate_topology(args)
        else:
            topology = tpg.Tofu.generate_topology(args)
    elif topo_type == 'arrangement':
        topology = tpg.Arrangement.generate_topology(args)
    elif topo_type == 'kautz':
        topology = tpg.Kautz.generate_topology(args)
    elif topo_type == 'dragonfly':
        topology = tpg.CompleteDragonfly.generate_topology(args)
    elif topo_type == 'cascade':
        topology = tpg.Cascade.generate_topology(args)
    elif topo_type == 'random':
        topology = tpg.RandomTopo.generate_topology(args)
    elif topo_type == 'load':
        topology = tpg.LoadTopo.load(args)
    else:
        exit('ERR: unknown/unsupported topology (%s) requested' % topo_type)

    return (topology, appendix)


def _write_topology(args, topology, supplement_material={}):
    if not isinstance(args, dict):
        exit('ERR: invalid function parameter type(s)')
    if not isinstance(topology, dict):
        exit('ERR: no topology given which could be written to file')

    tpg.IO.write(topology, args, supplement_material)


if __name__ == '__main__':
    args = _init_gen()

    profiling_reqested = args.get('profile')
    if profiling_reqested:
        print()
        pr = Profile()
        pr.enable()

    topology, supplement_material = _topo_generation(args)
    _write_topology(args, topology, supplement_material)

    if profiling_reqested:
        pr.disable()
        s = StringIO()
        ps = Stats(pr, stream=s).sort_stats('cumulative')
        ps.print_stats()
        print('\n\n%s' % s.getvalue())

    exit(0)
