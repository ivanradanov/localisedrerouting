import diskcache as dc
import os
import sys
import pathlib
import copy
import random
import time
import argparse
import threading
import itertools
import statistics
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from multiprocessing import Pool
from collections import OrderedDict

import IPython
def embed():
    if _DEBUG_:
        IPython.embed()

from . import partition
from . import routing_eval
from . import updn_routing
from . import hier_nue
from . import fault_gen
from . import util
from .util import ilog, vlog, dlog

fig_dpi = 400

results_path = os.path.join(pathlib.Path.home(), 'Research', 'hier_rout', 'experiment_results')

nue_routes_cache_path = os.path.join(pathlib.Path.home(),
                                     'Research', 'hier_rout',
                                     'caches', 'nue_routes' + hier_nue.global_ver)
nue_routes_cache = dc.Cache(str(nue_routes_cache_path), eviction_policy='none')

results_names = []

############################################
################ St ver3 ex ################
############################################

ver = '4.1'

st_v3_ex_id = 'local_nue_single_layer_local_nue_st_v_' + ver + '_' + hier_nue.local_ver
results_names.append(st_v3_ex_id)
st_v3_ex_id_with_ln_classes = 'local_nue_single_layer_local_nue_st_v_' + ver + '_with_ln_class_' + hier_nue.local_ver
results_names.append(st_v3_ex_id_with_ln_classes)

def generate_all_st_v3_ex_tests(topology_argss, layer_nums, target_sizes, ra_expansion_types):
    argss = []
    for topology_args in topology_argss:
        topology, failures = all_single_component_failures(topology_args)
        random.shuffle(failures)
        failures = failures[:target_data_points]
        one_section = len(failures) // process_num_g
        failures = failures[process_id * one_section : (process_id + 1) * one_section]
        for layer_num in layer_nums:
            for target_size in target_sizes:
                for ra_expansion_type in ra_expansion_types:
                    argss.append([[topology, failure, layer_num, target_size, ra_expansion_type]
                                  for failure in failures])
    # interleave all experiments with regards to the four above parameters
    argss = [x for x in itertools.chain(*itertools.zip_longest(*argss)) if x is not None]
    #argss = [val for tup in zip(*argss) for val in tup]
    return argss

def run_all_st_v3_ex_tests(process_num, *args):
    argss = generate_all_st_v3_ex_tests(*args)
    #one_section = len(argss) // process_num_g
    #argss = argss[process_id * one_section : (process_id + 1) * one_section]
    run_tests(process_num, _run_and_save_st_v3_ex, argss)

def all_single_component_failure_st_v3_ex_args(topology_args, layer_num, target_size):
    topology, failures = all_single_component_failures(topology_args)
    random.shuffle(failures)
    return [[topology, failure, layer_num, target_size] for failure in failures]

def _run_and_save_st_v3_ex(args):
    run_and_save_st_v3_ex(*args)

def run_and_save_st_v3_ex(topology, failure, layer_num, target_size, ra_expansion_type):
    print('.', end='', file = sys.stderr, flush = True)

    cache = results[st_v3_ex_id]
    cache_w_ln = results[st_v3_ex_id_with_ln_classes]

    cache_key = (topology.graph['gen_params'], failure, layer_num, target_size, ra_expansion_type)
    if cache_key in cache and 'ended' in cache[cache_key]:
        return

    res = {}
    res['original_topology'] = topology.graph['gen_params']
    res['failure'] = failure
    res['layer_num'] = layer_num
    res['target_size'] = target_size
    res['ra_expansion_type'] = ra_expansion_type

    tmp = nue_routes_cache.get((topology.graph['gen_params'], layer_num))
    if tmp == None:
        raise Exception('why')
    topology, network, cdg, routes, tree = tmp[0]
    res['original_nue_time'] = tmp[1]
    nodes = tmp[2]

    ln = hier_nue.LocalNue(topology, network, cdg, routes,
                           spanning_tree = tree,
                           target_size = target_size,
                           ra_expansion_type = ra_expansion_type)

    if 'random' in topology.graph['gen_params']:
        links = [link for link in topology.edges()
                 if not (topology.nodes[link[0]]['terminal'] or topology.nodes[link[1]]['terminal'])]
        failure['es'] = [links[ei] for ei in failure['es']]

    start_time = time.process_time()
    try:
        ln.reroute_alt_st_v3_preprocess(failure)
    except:
        res['failed'] = True
        cache.set(cache_key, res)
        cache_w_ln.set(cache_key, res)
        print('\nEXCEPTION OCCURED LOCAL NUE PREPROCESS\n', end='', file = sys.stderr, flush = True)
        embed()
        return
    end_time = time.process_time()

    res['local_nue_eval_package'] = ln.get_eval_package()
    res['local_nue_preprocess_time'] = end_time - start_time

    cache.set(cache_key, res)

    start_time = time.process_time()
    try:
        ln.reroute()
    except:
        res['failed'] = True
        cache.set(cache_key, res)
        cache_w_ln.set(cache_key, res)
        print('\nEXCEPTION OCCURED LOCAL NUE REROUTE\n', end='', file = sys.stderr, flush = True)
        embed()
        return
    end_time = time.process_time()

    res['local_nue_eval_package'] = ln.get_eval_package()
    res['local_nue_rerouting_time'] = end_time - start_time
    res['local_nue_time'] = res['local_nue_rerouting_time'] + res['local_nue_preprocess_time']

    res['ended'] = True

    i = routing_eval.edge_forwarding_indices(ln.routes, ln.network)
    res['efi_stats'] = get_statistics(i)

    cache.set(cache_key, res)

    res['local_nue_class'] = ln
    res['efi'] = i

    cache_w_ln.set(cache_key, res)




############################################
################ Global nue ################
############################################

ver = '4.1'

global_nue_ex_id = 'global_nue_' + ver + '_' + hier_nue.global_ver
results_names.append(global_nue_ex_id)

global_nue_ex_id_with_data = 'global_nue_' + ver + '_with_data_' + hier_nue.global_ver
results_names.append(global_nue_ex_id_with_data)

def generate_all_global_nue_ex_tests(topology_argss, layer_nums):
    argss = []
    for topology_args in topology_argss:
        topology, failures = all_single_component_failures(topology_args)
        random.shuffle(failures)
        failures = failures[:target_data_points]
        one_section = len(failures) // process_num_g
        failures = failures[process_id * one_section : (process_id + 1) * one_section]
        for layer_num in layer_nums:
            argss.append([[topology, failure, layer_num]
                          for failure in failures])
    # interleave all experiments with regards to the above parameters
    argss = [x for x in itertools.chain(*itertools.zip_longest(*argss)) if x is not None]
    #argss = [val for tup in zip(*argss) for val in tup]
    return argss

def run_all_global_nue_ex_tests(process_num, *args):
    argss = generate_all_global_nue_ex_tests(*args)
    #one_section = len(argss) // process_num_g
    #argss = argss[process_id * one_section : (process_id + 1) * one_section]
    run_tests(process_num, _run_and_save_global_nue_ex, argss)

def _run_and_save_global_nue_ex(args):
    run_and_save_global_nue_ex(*args)

def run_and_save_global_nue_ex(topology, failure, layer_num):
    print('.', end='', file = sys.stderr, flush = True)

    cache = results[global_nue_ex_id]
    cache_w_d = results[global_nue_ex_id_with_data]

    cache_key = (topology.graph['gen_params'], failure, layer_num)
    if cache_key in cache and 'ended' in cache[cache_key]:
        return
    print('-', end='', file = sys.stderr, flush = True)

    res = {}
    res['original_topology'] = topology.graph['gen_params']
    res['failure'] = failure
    res['layer_num'] = layer_num

    if 'random' in topology.graph['gen_params']:
        links = [link for link in topology.edges()
                 if not (topology.nodes[link[0]]['terminal'] or topology.nodes[link[1]]['terminal'])]
        failure['es'] = [links[ei] for ei in failure['es']]

    topology = topology.copy()

    def remove_failed_topo(topology, failure):
        for v, u in failure['es']:
            topology.remove_edge(v, u)

        for node in failure['vs']:
            topology.remove_node(node)

        isolated = []
        # remove isolated terminal nodes
        for node in list(topology.nodes()):
            if topology.nodes[node]['terminal'] and len(topology.adj[node]) == 0:
                topology.remove_node(node)

    remove_failed_topo(topology, failure)
    all_nodes = [k for k, v in topology.nodes(data=True) if v['terminal']]
    nodes_num = len(all_nodes) // layer_num
    nodes = random.sample(all_nodes, nodes_num)


    start_time = time.process_time()
    try:
        out = hier_nue.nue_routing_single_layer(topology, nodes)
    except:
        res['failed'] = True
        cache.set(cache_key, res)
        res['nodes'] = nodes
        cache_w_d.set(cache_key, res)
        print('\nEXCEPTION OCCURED LOCAL NUE PREPROCESS\n', end='', file = sys.stderr, flush = True)
        embed()
        return
    end_time = time.process_time()

    res['nue_time'] = end_time - start_time
    res['ended'] = True

    _, network, _, routes, _ = out
    i = routing_eval.edge_forwarding_indices(routes, network)
    res['efi_stats'] = get_statistics(i)

    cache.set(cache_key, res)

    res['nodes'] = nodes
    res['output'] = out
    res['efi'] = i

    cache_w_d.set(cache_key, res)

##############################################
################ Routing eval ################
##############################################

def get_statistics(i):
    q1, med, q3 = statistics.quantiles(i)
    stats = {}
    stats['med'] = med
    stats['q1'] = q1
    stats['q3'] = q3
    stats['whislo'] = min(i)
    stats['whishi'] = max(i)
    return stats

def evaluate_global_nue_routings(process_num):
    argss = list(results[global_nue_ex_id_with_data].iterkeys())
    #for args in argss: evaluate_global_nue_routing(args)
    run_tests(process_num, evaluate_global_nue_routing, argss)

def evaluate_global_nue_routing(cache_key):
    if cache_key[2] != 1:
        return

    cres = results[global_nue_ex_id_with_data].get(cache_key)
    print(cache_key)

    if 'ended' not in cres:
        return

    _, network, _, routes, _ = cres['output']

    res = results[global_nue_ex_id].get(cache_key)
    if 'efi_stats' not in res or type(res['efi_stats']) is not dict:
        print('|', end='', file = sys.stderr, flush = True)
        i = routing_eval.edge_forwarding_indices(routes, network)
        res['efi_stats'] = get_statistics(i)
        results[global_nue_ex_id].set(cache_key, res)

def evaluate_local_nue_routings(process_num):
    argss = list(results[st_v3_ex_id_with_ln_classes].iterkeys())
    #for args in argss: evaluate_local_nue_routing(args)
    run_tests(process_num, evaluate_local_nue_routing, argss)

def evaluate_local_nue_routing(cache_key):
    cres = results[st_v3_ex_id_with_ln_classes].get(cache_key)

    if cache_key[2] != 1:
        return

    if 'ended' not in cres:
        return

    #print('.', end='', file = sys.stderr, flush = True)
    ln = cres['local_nue_class']

    res = results[st_v3_ex_id].get(cache_key)
    #if True: #'efi_stats' not in res:
    if type(res['efi_stats']) is not dict:

        print('-', end='', file = sys.stderr, flush = True)
        if 'efi' in cres:
            i = cres['efi']
        else:
            i = routing_eval.edge_forwarding_indices(ln.routes, ln.network)
            cres['efi'] = i
            results[st_v3_ex_id_with_ln_classes].set(cache_key, cres)


        if 'efi' in res:
            del res['efi']

        res['efi_stats'] = get_statistics(i)

        results[st_v3_ex_id].set(cache_key, res)

#########################################
################ Figures ################
#########################################

figures_cache_path = os.path.join(pathlib.Path.home(),
                                  'Research', 'hier_rout',
                                  'caches', 'figures')
figures_cache = dc.Cache(str(figures_cache_path), eviction_policy='none')

def routing_eval_time_comparison_figure():

    res_local = results[st_v3_ex_id]
    res_local_w = results[st_v3_ex_id_with_ln_classes]
    res_global = results[global_nue_ex_id]

    title = 'routing_eval_time_comparison_figure'

    if title not in figures_cache:
        figures_cache[title] = 0
    figures_cache[title] += 1
    title = title + '_' + str(figures_cache[title])

    topo = '--nt 1080 torus --dim 5 6 6'

    layer_num = 1
    step = 10
    target_sizes = list(range(0, 190, step))

    res = res_local
    times = {target_size: [res[x]['local_nue_time']
                           for x in res.iterkeys()
                           if topo == x[0] and 1 == x[2] and target_size == x[3] and 'ended' in res[x] and 'efi_stats' in res[x]]
             for target_size in target_sizes}
    print([len(times) for times in times.values()])
    times = OrderedDict(sorted(times.items()))
    x1 = [key for key in times.keys()]
    y1 = [val for val in times.values()]

    res = res_global
    global_mean_time = statistics.mean([res[x]['nue_time'] for x in res.iterkeys()
                                   if topo == x[0] and 1 == x[2] and 'ended' in res[x] and 'efi_stats' in res[x]])

    res = res_local
    statss = OrderedDict()
    for target_size in target_sizes:
        stats = {
            'med': [],
            'q1': [],
            'q3': [],
            'whislo': [],
            'whishi': [],
        }
        num = 0
        limit = hier_nue.inf
        keys = list(res.iterkeys())
        random.shuffle(keys)
        for x in keys:
            if num >= limit:
                break

            if not(topo == x[0] and 1 == x[2] and target_size == x[3]):
                continue

            r = res[x]
            print('.', end='', file = sys.stderr, flush = True)

            if not('ended' in r and 'efi_stats' in r and type(r['efi_stats']) is dict):
                continue

            print('-', end='', file = sys.stderr, flush = True)

            efi = r['efi_stats']

            for s, d in efi.items():
                stats[s].append(d)

            '''
            stats['med'].append(med)
            stats['q1'].append(q1)
            stats['q3'].append(q3)
            stats['whislo'].append(min(efi))
            stats['whishi'].append(max(efi))
            '''

            num += 1

        stats = {stat: statistics.mean(data) for stat, data in stats.items()}

        statss[target_size] = stats

    ##############################
    res = res_global
    global_stats = {
        'med': [],
        'q1': [],
        'q3': [],
        'whislo': [],
        'whishi': [],
    }
    for x in res.iterkeys():
        if not(topo == x[0] and 1 == x[2]):
            continue

        r = res[x]

        if not('ended' in r and 'efi_stats' in r and type(r['efi_stats']) is dict):
            continue

        efi = r['efi_stats']

        for s, d in efi.items():
            global_stats[s].append(d)
    global_stats = {stat: statistics.mean(data) for stat, data in global_stats.items()}

    ##############################
    original_stats = {
        'med': [],
        'q1': [],
        'q3': [],
        'whislo': [],
        'whishi': [],
    }
    _, network, _, routes, _ = nue_routes_cache.get((topo, 1))[0]
    original_stats = get_statistics(routing_eval.edge_forwarding_indices(routes, network))

    ##############################

    xaxis = target_sizes + [target_sizes[-1] + step * 2] + [target_sizes[-1] + step * 3]

    fig, (ax, ax2) = plt.subplots(nrows=2)
    ax.violinplot(y1 + ([[global_mean_time]] * 2),
                  showmeans = True, showmedians = False, showextrema = True,
                  widths = step,
                  positions = xaxis
    )
    ax.plot(target_sizes, [global_mean_time] * len(target_sizes))
    ax.axhline(global_mean_time, color='tab:orange')

    #ax2 = ax.twinx()
    ax2.set_ylabel('Edge forwarding index')
    ax2.set_yscale('log')

    y2 = list(statss.values())
    x2 = list(statss.keys())
    print(y2, x2)
    bplot_local = ax2.bxp(
        y2,
        positions = x2,
        showfliers = False,
        shownotches = False,
        showmeans = False,
        patch_artist = True,
        widths = step * 2/3,
        medianprops = dict(color='black'),
    )

    bplot_global = ax2.bxp(
        [global_stats],
        positions = [target_sizes[-1] + step * 2],
        showfliers = False,
        shownotches = False,
        showmeans = False,
        patch_artist = True,
        widths = step * 2/3,
        medianprops = dict(color='black'),
    )

    colors = ['tab:blue', 'tab:orange']
    for bplot, color in zip((bplot_local, bplot_global), colors):
        for patch in bplot['boxes']:
            patch.set_facecolor(color)

    ax2.bxp(
        [original_stats],
        positions = [target_sizes[-1] + step * 3],
        showfliers = False,
        shownotches = False,
        showmeans = False,
        widths = step * 2/3,
        medianprops = dict(color='black'),
    )

    xlabels = [str(x) if (x / 10) % 2 == 0 else '' for x in xaxis]
    xlabels1 = [str(x) if (x / 10) % 2 == 0 else '' for x in target_sizes]
    xlabels[-2] = 'global'
    xlabels[-1] = 'original'
    ax2.set_xticks(xaxis)
    ax2.set_xticklabels(xlabels, rotation = 30)
    ax.set_xticks(target_sizes)
    ax.set_xticklabels(xlabels1, rotation = 30)

    handles = [mpatches.Patch(color='tab:blue'), mpatches.Patch(color='tab:orange')] #, mpatches.Patch(color='tab:green')]
    ax.legend(handles,
              ['local time', 'global avarage time'], #, 'local routing edge forwarding index'],
              loc = 'lower right')

    ax2.set_xlabel('Recalculation area target size')
    ax.set_ylabel('Time (s)')

    ax.set_title('Runtime and routing performance trade-off')

    plt.savefig('./figs/' + title,
                dpi = fig_dpi,
                bbox_inches = 'tight',
    )

def various_topology_comparison_figure_normalised():

    res_local = results[st_v3_ex_id]
    res_global = results[global_nue_ex_id]

    title = 'various_topology_comparison_normalised'

    if title not in figures_cache:
        figures_cache[title] = 0
    figures_cache[title] += 1
    title = title + '_' + str(figures_cache[title])

    topos = ['--nt 1080 torus --dim 5 6 6',
             '--nt 1100 hyperx --hxL 3 --hxS 4 5 5 --hxK 4 4 4',
             '--nt 1000 k-ary-n --tk 10 --tn 3',
             '--nt 896 hypercube --dim 7',
             '--nt 1050 random --ns 150',
             'dragonfly --da 7',
             'cascade --cg 1']



    res = res_global
    times = {topo: [res[x]['nue_time']
                     for x in res.iterkeys() if topo == x[0] and 4 == x[2] and 'ended' in res[x]]
              for topo in {x[0] for x in res.iterkeys() if x[0] in topos}}
    times = OrderedDict(sorted(times.items()))

    mean_times = {topo: statistics.mean(t) for topo, t in times.items()}
    scaling_factor = {topo: 1 / mt for topo, mt in mean_times.items()}


    res = res_local
    times = {topo: [res[x]['local_nue_time'] * scaling_factor[topo]
                     for x in res.iterkeys() if topo == x[0] and 4 == x[2] and 0 == x[3] and 'ended' in res[x]]
              for topo in {x[0] for x in res.iterkeys() if x[0] in topos}}
    times = OrderedDict(sorted(times.items()))
    x1 = [key for key in times.keys()]
    y1 = [val for val in times.values()]

    res = res_global
    times = {topo: [res[x]['nue_time'] * scaling_factor[topo]
                     for x in res.iterkeys() if topo == x[0] and 4 == x[2] and 'ended' in res[x]]
              for topo in {x[0] for x in res.iterkeys() if x[0] in topos}}
    times = OrderedDict(sorted(times.items()))
    x2 = [key for key in times.keys()]
    y2 = [val for val in times.values()]


    fig, ax = plt.subplots()
    ax.violinplot(y1,
                  showmeans = True, showmedians = False, showextrema = True,
                  #widths = [x * 0.1 for x in x1],
                  #positions = x1
    )
    ax.violinplot(y2,
                  showmeans = True, showmedians = False, showextrema = True,
                  #widths = [x * 0.1 for x in x2],
                  #positions = x2
    )

    xlabels = [topo.split()[0] if topo[0] != '-' else topo.split()[2] for topo in sorted(topos)]
    xaxis = list(range(1, 1 + len(xlabels)))
    plt.xticks(xaxis, xlabels, rotation = 20)

    plt.yticks(np.arange(0, 1.1, step = 0.2))
    plt.ylim(top = 1.3)

    handles = [mpatches.Patch(color='tab:blue'), mpatches.Patch(color='tab:orange')]
    ax.legend(handles, ['local', 'global'], loc = 'upper right')

    #ax.set_xlabel('Topology')
    ax.set_ylabel('Relative time')

    ax.set_title('Execution time for various topologies, normalised')

    plt.savefig('./figs/' + title,
                dpi = fig_dpi,
                bbox_inches = 'tight',
    )

def various_topology_comparison_figure_equal_terminals():

    res_local = results[st_v3_ex_id]
    res_global = results[global_nue_ex_id]

    title = 'various_topology_comparison_scaled_for_equal_terminals'

    if title not in figures_cache:
        figures_cache[title] = 0
    figures_cache[title] += 1
    title = title + '_' + str(figures_cache[title])

    topos = ['--nt 1080 torus --dim 5 6 6',
             '--nt 1100 hyperx --hxL 3 --hxS 4 5 5 --hxK 4 4 4',
             '--nt 1000 k-ary-n --tk 10 --tn 3',
             '--nt 896 hypercube --dim 7',
             '--nt 1050 random --ns 150',
             'dragonfly --da 7',
             'cascade --cg 1']

    terminal_nodess = {topo: partition.topology_terminal_num(partition.non_partitioned_topology_nx(topo)) for topo in topos}
    target_terminal_node_num = 1000
    scaling_factor = {topo: target_terminal_node_num / terminal_nodes for topo, terminal_nodes in terminal_nodess.items()}


    res = res_local
    times = {topo: [res[x]['local_nue_time'] * scaling_factor[topo]
                     for x in res.iterkeys() if topo == x[0] and 4 == x[2] and 0 == x[3] and 'ended' in res[x]]
              for topo in {x[0] for x in res.iterkeys() if x[0] in topos}}
    times = OrderedDict(sorted(times.items()))
    x1 = [key for key in times.keys()]
    y1 = [val for val in times.values()]

    res = res_global
    times = {topo: [res[x]['nue_time'] * scaling_factor[topo]
                     for x in res.iterkeys() if topo == x[0] and 4 == x[2] and 'ended' in res[x]]
              for topo in {x[0] for x in res.iterkeys() if x[0] in topos}}
    times = OrderedDict(sorted(times.items()))
    x2 = [key for key in times.keys()]
    y2 = [val for val in times.values()]


    fig, ax = plt.subplots()
    ax.violinplot(y1,
                  showmeans = True, showmedians = False, showextrema = True,
                  #widths = [x * 0.1 for x in x1],
                  #positions = x1
    )
    ax.violinplot(y2,
                  showmeans = True, showmedians = False, showextrema = True,
                  #widths = [x * 0.1 for x in x2],
                  #positions = x2
    )

    xlabels = [topo.split()[0] if topo[0] != '-' else topo.split()[2] for topo in sorted(topos)]
    xaxis = list(range(1, 1 + len(xlabels)))
    plt.xticks(xaxis, xlabels, rotation = 20)

    handles = [mpatches.Patch(color='tab:blue'), mpatches.Patch(color='tab:orange')]
    ax.legend(handles, ['local', 'global'], loc = 'upper right')

    #ax.set_xlabel('Topology')
    ax.set_ylabel('Time (s)')

    ax.set_title('Execution time for various topologies, scaled for equal terminal numbers')

    plt.savefig('./figs/' + title,
                dpi = fig_dpi,
                bbox_inches = 'tight',
    )

def torus_different_layers_experiment():

    res_local = results[st_v3_ex_id]
    res_global = results[global_nue_ex_id]

    title = 'torus_different_layers'

    if title not in figures_cache:
        figures_cache[title] = 0
    figures_cache[title] += 1
    title = title + '_' + str(figures_cache[title])

    layer_nums = [1, 2, 4, 8, 16, 32]
    topo = partition.args_for_multi_terminal('torus --dim 5 6 6')

    res = res_local
    times = {layer_num: [res[x]['local_nue_time']
                         for x in res.iterkeys() if topo == x[0] and layer_num == x[2] and 0 == x[3]]
             for layer_num in layer_nums}
    times = OrderedDict(sorted(times.items()))
    x1 = [key for key in times.keys()]
    y1 = [val for val in times.values()]

    res = res_global
    times = {layer_num: [res[x]['nue_time']
                         for x in res.iterkeys() if topo == x[0] and layer_num == x[2]]
             for layer_num in layer_nums}
    times = OrderedDict(sorted(times.items()))
    x2 = [key for key in times.keys()]
    y2 = [val for val in times.values()]


    fig, ax = plt.subplots()
    ax.violinplot(y1,
                  showmeans = True, showmedians = False, showextrema = True,
                  #widths = [x * 0.1 for x in x1],
                  positions = range(1, len(layer_nums) + 1)
    )
    ax.violinplot(y2,
                  showmeans = True, showmedians = False, showextrema = True,
                  #widths = [x * 0.1 for x in x2],
                  positions = range(1, len(layer_nums) + 1)
    )

    handles = [mpatches.Patch(color='tab:blue'), mpatches.Patch(color='tab:orange')]
    ax.legend(handles, ['local', 'global'], loc = 'upper right')

    plt.xticks(list(range(1, len(layer_nums) + 1)), [str(layer_num) for layer_num in layer_nums])

    ax.set_xlabel('Number of layers')
    ax.set_ylabel('Time (s)')

    ax.set_title('Execution time for networks with different number of layers')

    plt.savefig('./figs/' + title,
                dpi = fig_dpi,
                bbox_inches = 'tight',
    )

def torus_scaling_experiment_figure(log = True):

    res_local = results[st_v3_ex_id]
    res_global = results[global_nue_ex_id]

    title = 'scaling_experiment_3d_toruses_4_layer' + ('log' if log else 'lin')

    if title not in figures_cache:
        figures_cache[title] = 0
    figures_cache[title] += 1
    title = title + '_' + str(figures_cache[title])


    res = res_local
    times = {int(topo.split()[1]): [res[x]['local_nue_time']
                     for x in res.iterkeys() if topo == x[0] and 4 == x[2] and 0 == x[3] and 'ended' in res[x]]
              for topo in {x[0] for x in res.iterkeys() if 'torus' in x[0]}}
    times = OrderedDict(sorted(times.items()))
    x1 = [key for key in times.keys()]
    y1 = [val for val in times.values()]

    res = res_global
    times = {int(topo.split()[1]): [res[x]['nue_time']
                     for x in res.iterkeys() if topo == x[0] and 4 == x[2] and 'ended' in res[x]]
              for topo in {x[0] for x in res.iterkeys() if 'torus' in x[0]}}
    times = OrderedDict(sorted(times.items()))
    x2 = [key for key in times.keys()]
    y2 = [val for val in times.values()]


    fig, ax = plt.subplots()
    ax.violinplot(y1,
                  showmeans = True, showmedians = False, showextrema = True,
                  widths = [x * 0.1 for x in x1],
                  positions = x1)
    ax.violinplot(y2,
                  showmeans = True, showmedians = False, showextrema = True,
                  widths = [x * 0.1 for x in x2],
                  positions = x2)

    handles = [mpatches.Patch(color='tab:blue'), mpatches.Patch(color='tab:orange')]
    ax.legend(handles, ['local', 'global'], loc = 'upper left')


    if log:
        ax.set_yscale('log')
        ax.set_xscale('log')
        ax.set_xticks(x1)
        ax.set_xticklabels(x1, rotation = 45)
        #ax.set_xticklabels(x1)
        ax.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        ax.get_xaxis().set_tick_params(which='minor', size=0)
        ax.get_xaxis().set_tick_params(which='minor', width=0)

    ax.set_xlabel('Number of terminal nodes')
    ax.set_ylabel('Time (s)')

    ax.set_title('Execution time for 4 layer 3D torus networks, ' + ('logarithmic' if log else 'linear') + ' axes')

    plt.savefig('./figs/' + title,
                dpi = fig_dpi,
                bbox_inches = 'tight',
    )

###################################################
################ Performance tests ################
###################################################

def _run_performace_test(args):
    run_performance_test(*args)

def run_performance_test(topology, failure, layer_num, target_size, ra_expansion_type, tmp):
    topology, network, cdg, routes, tree = tmp[0]
    ln = hier_nue.LocalNue(topology, network, cdg, routes,
                           spanning_tree = tree,
                           target_size = target_size,
                           ra_expansion_type = ra_expansion_type)

    start_time = time.process_time()
    res = {}
    try:
        ln.reroute_alt_st_v3_preprocess(failure)
    except:
        res['failed'] = True
        cache.set(cache_key, res)
        cache_w_ln.set(cache_key, res)
        print('\nEXCEPTION OCCURED LOCAL NUE PREPROCESS\n', end='', file = sys.stderr, flush = True)
        embed()
        return
    end_time = time.process_time()

    res['local_nue_preprocess_time'] = end_time - start_time
    start_time = time.process_time()
    try:
        ln.reroute()
    except:
        res['failed'] = True
        cache.set(cache_key, res)
        cache_w_ln.set(cache_key, res)
        print('\nEXCEPTION OCCURED LOCAL NUE REROUTE\n', end='', file = sys.stderr, flush = True)
        embed()
        return
    end_time = time.process_time()

    res['local_nue_rerouting_time'] = end_time - start_time
    res['local_nue_time'] = res['local_nue_rerouting_time'] + res['local_nue_preprocess_time']

    print(res['local_nue_time'], ',', flush = True, file = sys.stderr)

def test_multi_vs_single_performance():
    util.log_mode = util.___NONE_

    default_topology_argss = ['torus --dim 4 4 4']

    default_topology_argss = [partition.args_for_multi_terminal(t) for t in default_topology_argss]

    default_layer_nums = [4]

    #default_target_sizes = [i * 10 for i in range(6)]
    default_target_sizes = [30]

    #default_ra_expansion_types = ['topology', 'tree']
    default_ra_expansion_types = ['topology']

    argss = generate_all_st_v3_ex_tests(
                            default_topology_argss,
                            default_layer_nums,
                            default_target_sizes,
                            default_ra_expansion_types)
    args = argss[0]

    tmp = nue_routes_cache.get((args[0].graph['gen_params'], args[2]))
    if tmp == None:
        raise Exception('why')

    args = args + [tmp]

    start_time = time.process_time()
    run_performance_test(*args)
    end_time = time.process_time()
    print(end_time - start_time)



#######################################
################ Utils ################
#######################################

results = {}
for name in results_names:
    # name = name + '_' + hier_nue.local_ver
    results[name] = dc.Cache(str(os.path.join(results_path, name)), size_limit = hier_nue.inf, eviction_policy='none')

def make_single_arg(fun):
    return lambda args: fun(*args)

def run_tests(process_num, test_fun, tests, debug = False):
    if debug or _DEBUG_:
        list(map(test_fun, tests))
        return
    # equivalent to map(test_fun, tests)
    with Pool(process_num) as p:
        p.map(test_fun, tests)

topos = {}

def all_single_component_failures(topology_args):
    if topology_args in topos:
        topology = topos[topology_args]
    else:
        topology = partition.non_partitioned_topology_nx(topology_args)
        topos[topology_args] = topology

    failures = []
    failures += [{'vs': [node], 'es': []} for node in topology.nodes() if not topology.nodes[node]['terminal']]
    if 'random' in topology_args:
        num_links = len([0 for link in topology.edges() if not (topology.nodes[link[0]]['terminal'] or topology.nodes[link[1]]['terminal'])])
        failures += [{'vs': [], 'es': [link]} for link in range(num_links)]
    else:
        failures += [{'vs': [], 'es': [link]} for link in topology.edges() if not (topology.nodes[link[0]]['terminal'] or topology.nodes[link[1]]['terminal'])]
    return topology, failures

def preload_nue_routing(args):
    print('.', end='', file = sys.stderr, flush = True)

    topology_args = args[0]
    layer_num = args[1]

    tmp = nue_routes_cache.get((topology_args, layer_num))
    if tmp == None:
        topology = partition.non_partitioned_topology_nx(topology_args)
        all_nodes = [k for k, v in topology.nodes(data=True) if v['terminal']]
        nodes_num = len(all_nodes) // layer_num
        nodes = random.sample(all_nodes, nodes_num)
        start_time_nue = time.process_time()
        tmp = hier_nue.nue_routing_single_layer(topology, nodes)
        end_time_nue = time.process_time()
        tmp = (tmp, end_time_nue - start_time_nue, nodes)
        nue_routes_cache.set((topology_args, layer_num), tmp)

def preload_nue_routings(process_num, topology_argss, layer_nums):
    argss = []
    for topology_args in topology_argss:
        for layer_num in layer_nums:
            argss.append([topology_args, layer_num])
    run_tests(process_num, preload_nue_routing, argss)

# around 2000 terminals
default_topology_argss = [
    'torus --dim 7 7 7',
    'hyperx --hxL 3 --hxS 5 5 6 --hxK 5 5 5',
    'k-ary-n --tk 13 --tn 3',
    'tofu --dims 30',
    'hypercube --dim 8',
    'random --ns 300',
    'dragonfly --da 10 --dg 40',
    'cascade --cg 1',
]

# for torus scaling experiments
default_topology_argss = list(itertools.chain.from_iterable((
    'torus --dim ' + str(x) + ' ' + str(x) + ' ' + str(x + 1),
    'torus --dim ' + str(x) + ' ' + str(x + 1) + ' ' + str(x + 1),
    'torus --dim ' + str(x + 1) + ' ' + str(x + 1) + ' ' + str(x + 1),
) for x in range(3, 8)))

# around 1000 terminals
default_topology_argss = [
    'torus --dim 5 6 6',
    'hyperx --hxL 3 --hxS 4 5 5 --hxK 4 4 4',
    'k-ary-n --tk 10 --tn 3',
    #'tofu --dims 16',
    'hypercube --dim 7',
    'random --ns 150',
    'dragonfly --da 7',
    'cascade --cg 1',
]

default_topology_argss = [
    'torus --dim 5 6 6',
]

default_topology_argss = [
    'random --ns 150',
]

default_topology_argss = [partition.args_for_multi_terminal(t) for t in default_topology_argss]

default_layer_nums = [1, 4, 8]
default_layer_nums = [2 ** i for i in range(6)]
default_layer_nums = [1]
default_layer_nums = [4]

#default_target_sizes = [i * 10 for i in range(6)]
default_target_sizes = list(range(180, 190, 10))
default_target_sizes = list(range(0, 180, 10))
default_target_sizes = list(range(0, 190, 10))
default_target_sizes = [0]

#default_ra_expansion_types = ['topology', 'tree']
default_ra_expansion_types = ['topology']

target_data_points = 108

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--num_processes')
    parser.add_argument('--global_num_processes')
    parser.add_argument('--seed')
    parser.add_argument('--process_id')
    parser.add_argument('--perf_test', action='store_true')
    parser.add_argument('--preload', action='store_true')
    parser.add_argument('--evaluate', action='store_true')
    parser.add_argument('--debug', action='store_true')
    args, topo_args = parser.parse_known_args()

    if not args.debug:
        util.log_mode = util.___NONE_

    if args.seed is not None:
        random.seed(args.seed)

    if args.process_id is not None:
        global process_id
        process_id = int(args.process_id)
        global process_num_g
        process_num_g = int(args.global_num_processes)

    process_num = int(args.num_processes)

    if args.perf_test:
        test_multi_vs_single_performance()
        sys.exit()

    if args.preload:
        print('preloading nue routings', file = sys.stderr)
        preload_nue_routings(process_num, default_topology_argss, default_layer_nums)
        print(file = sys.stderr)
        sys.exit()

    if args.evaluate:
        evaluate_global_nue_routings(process_num)
        evaluate_local_nue_routings(process_num)
        sys.exit()

    global _DEBUG_
    _DEBUG_ = args.debug

    print('executing tests', file = sys.stderr)
    if True:
        run_all_st_v3_ex_tests(process_num,
                               default_topology_argss,
                               default_layer_nums,
                               default_target_sizes,
                               default_ra_expansion_types)
    elif True:
        run_all_global_nue_ex_tests(process_num,
                                    default_topology_argss,
                                    default_layer_nums)
    print(file = sys.stderr)

    sys.exit()
