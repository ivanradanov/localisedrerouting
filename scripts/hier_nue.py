import sys
import random
import time
import heapq
import statistics
import networkx as nx
from .util import ilog, dlog, vlog
import copy

from collections import OrderedDict

global_ver = 'v2'
local_ver = 'v1.4.1-destination-based'

inf = sys.maxsize

# opposite edge
def opp(edge):
    if len(edge) == 2:
        return (edge[1], edge[0])
    else:
        return (edge[1], edge[0], edge[2])

def complete_cdg(network):
    vlog('computing complete cdg from network')
    cdg = nx.DiGraph()
    for (u, v) in network.edges():
        # do not add dependencies through terminal nodes because all traffic
        # originates and ends in terminal nodes and should not pass through
        # them otherwise
        if network.nodes[v]['terminal']:
            continue
        for w in network.adj[v]:
            cdg.add_edge((u, v), (v, w), state='unused')
    return cdg

def cycle_detection_prep(cdg):
    vlog('prepping cycle detection')
    cdg.graph['white'] = 1
    cdg.graph['cfgroupid'] = 1
    # cdg.graph['cfgroups'] = []

    for edge in cdg.edges():
        cdg.edges[edge]['omega'] = 0
        cdg.edges[edge]['push_counter'] = 0
        cdg.edges[edge]['omegastack'] = [0]
    for node in cdg.nodes():
        cdg.nodes[node]['omega'] = 0
        cdg.nodes[node]['omegastack'] = [0]
        cdg.nodes[node]['color'] = 0

def update_edge_omega(cdg, edge, new_id):
    cdg.edges[edge]['omega'] = new_id
    cdg.edges[edge]['omegastack'].append(new_id)

def update_node_omega(cdg, node, new_id):
    cdg.nodes[node]['omega'] = new_id
    cdg.nodes[node]['omegastack'].append(new_id)

def revert_edge_omega(cdg, edge):
    cdg.edges[edge]['omegastack'].pop()
    old_id = cdg.edges[edge]['omegastack'][-1]
    cdg.edges[edge]['omega'] = old_id
    if cdg.edges[edge]['omega'] == 0:
        cdg.edges[edge]['state'] = 'unused'
    elif cdg.edges[edge]['omega'] >= 1:
        cdg.edges[edge]['state'] = 'used'
    else:
        raise Exception('omega of -1 should not be possible when reverting')


def revert_node_omega(cdg, node):
    cdg.nodes[node]['omegastack'].pop()
    old_id = cdg.nodes[node]['omegastack'][-1]
    cdg.nodes[node]['omega'] = old_id

def change_cycle_group_id(cdg, old_id, new_id):
    assert(old_id > 0)
    assert(new_id > 0)
    for edge in cdg.edges():
        if cdg.edges[edge]['omega'] == old_id:
            update_edge_omega(cdg, edge, new_id)
    for node in cdg.nodes():
        if cdg.nodes[node]['omega'] == old_id:
            update_node_omega(cdg, node, new_id)

def get_new_cycle_group(cdg):
    new_group_id = cdg.graph['cfgroupid']
    cdg.graph['cfgroupid'] += 1
    return new_group_id

def is_cycle_free_with_push(cdg, edge):
    if cdg.edges[edge]['omega'] == 0:
        if is_cycle_free_with(cdg, edge):
            cdg.edges[edge]['push_counter'] = 1
            return 'added'
        else:
            cdg.edges[edge]['push_counter'] = 1
            return 'blocked'
    elif cdg.edges[edge]['omega'] >= 1:
        assert(cdg.edges[edge]['state'] == 'used')
        if cdg.edges[edge]['push_counter'] == 0:
            cdg.edges[edge]['push_counter'] = 2
        else:
            cdg.edges[edge]['push_counter'] += 1
        return 'already_existed'
    else:
        raise Exception('pushing a blocked edge should not happen')

# pop most recently added edge and revert states and omega
def is_cycle_free_with_pop(cdg, edge):
    if 'push_counter' not in cdg.edges[edge] or cdg.edges[edge]['push_counter'] == 0:
        return
    cdg.edges[edge]['push_counter'] -= 1
    if cdg.edges[edge]['push_counter'] > 0:
        return
    pop_id = cdg.edges[edge]['omega']
    if pop_id == -1:
        assert(cdg.edges[edge]['state'] == 'blocked')
        revert_edge_omega(cdg, edge)
    elif pop_id >= 1:
        assert(cdg.edges[edge]['state'] == 'used')
        if cdg.edges[edge]['added_type'] == 'new_id':
            for edge in cdg.edges():
                if cdg.edges[edge]['omega'] == pop_id:
                    assert(pop_id == cdg.edges[edge]['omegastack'][-1])
                    revert_edge_omega(cdg, edge)
            for node in cdg.nodes():
                if cdg.nodes[node]['omega'] == pop_id:
                    assert(pop_id == cdg.nodes[node]['omegastack'][-1])
                    revert_node_omega(cdg, node)
        elif cdg.edges[edge]['added_type'] == 'joined_id':
            revert_edge_omega(cdg, edge)
            cdg.edges[edge]['state'] = 'unused'
    else:
        assert(cdg.edges[edge]['state'] == 'unused')
        raise Exception('pop called on edge {} with unused state'.format(edge))

def is_cycle_free_with(cdg, edge):

	# optimized cycle detection
    #
    # this function tries to add the edge to the cdg (i.e. change its state to
    # 'used') and adjusts the states and omega accordingly

    if cdg.edges[edge]['omega'] == -1:
        assert(cdg.edges[edge]['state'] == 'blocked')
        return False
    elif cdg.edges[edge]['omega'] >= 1:
        assert(cdg.edges[edge]['state'] == 'used')
        return True
    elif cdg.edges[edge]['omega'] == 0:
        assert(cdg.edges[edge]['state'] == 'unused')
        if cdg.nodes[edge[0]]['omega'] != cdg.nodes[edge[1]]['omega']:
            cdg.edges[edge]['added_type'] = 'new_id'
            new_group_id = get_new_cycle_group(cdg)
            cdg.edges[edge]['state'] = 'used'
            update_edge_omega(cdg, edge, new_group_id)
            def update_ends(cdg, end, new_group_id):
                if cdg.nodes[end]['omega'] == 0:
                    update_node_omega(cdg, end, new_group_id)
                else:
                    change_cycle_group_id(cdg, cdg.nodes[end]['omega'], new_group_id)
            update_ends(cdg, edge[0], new_group_id)
            update_ends(cdg, edge[1], new_group_id)
            return True
        elif cdg.nodes[edge[0]]['omega'] == cdg.nodes[edge[1]]['omega']:
            if cdg.nodes[edge[0]]['omega'] == 0:
                cdg.edges[edge]['added_type'] = 'new_id'
                new_group_id = get_new_cycle_group(cdg)
                cdg.edges[edge]['state'] = 'used'
                update_edge_omega(cdg, edge, new_group_id)
                update_node_omega(cdg, edge[0], new_group_id)
                update_node_omega(cdg, edge[1], new_group_id)
                return True
            else:
                cdg.edges[edge]['added_type'] = 'joined_id'
                cdg.edges[edge]['state'] = 'used'
                if my_is_cycle_free(cdg, edge[0]):
                    update_edge_omega(cdg, edge, cdg.nodes[edge[0]]['omega'])
                    return True
                else:
                    update_edge_omega(cdg, edge, -1)
                    cdg.edges[edge]['state'] = 'blocked'
                    return False
        else:
            assert(False)
    else:
        assert(False)

def is_cycle_free(cdg):
    # find a lighter way to do this, the copy is needed because we cannot use
    # the same graph for checking the state, it causes endless recursion
    _cdg = cdg.copy()
    cdg = nx.subgraph_view(
        cdg, filter_edge = lambda u, v: _cdg.edges[u, v]['state'] == 'used')
    try:
        cycle = nx.find_cycle(cdg)
    except nx.NetworkXNoCycle:
        return True
    return False

def cycle_exists(g, v, white):
    g.nodes[v]['color'] = white + 1
    dlog('visiting {}'.format(v))
    for u in g.adj[v]:
        if g.edges[v, u]['state'] != 'used':
            continue
        if g.nodes[u]['color'] <= white:
            if cycle_exists(g, u, white):
                return True
        elif g.nodes[u]['color'] == white + 1:
            return True
    g.nodes[v]['color'] = white + 2
    return False

def my_is_cycle_free(g, v):
    white = g.graph['white']
    g.graph['white'] += 2

    return not cycle_exists(g, v, white)

# network here is a directed graph, cdg the complete cdg
# cdg is mutable, network is not
def nue_dijkstra(network, cdg, escape_paths, src_node):
    vlog('dijkstra from node {}'.format(src_node))

    if network.nodes[src_node]['terminal']:
        adjacent = list(network.adj[src_node])
        assert(len(adjacent) == 1)
        src_channel = (src_node, adjacent[0])
    else:
        assert(False, 'Not supported')
        ## need the removal logic afterwards
        src_channel = (None, src_node)
        cdg.add_node(src_channel)
        for node in network.adj[src_node]:
            cdg.add_edge(src_channel, (src_node, node))

    used_channel = core_dijkstra(network, cdg, src_channel, escape = False)
    for node in network:
        if node != src_node and used_channel[node] == None:
            break
    else:
        return used_channel
    # we have not reached all nodes, i.e. we had reached an impasse -> use
    # escape paths
    ilog('!!!!!!!! reached impasse, route using escape paths !!!!!!!!')
    return core_dijkstra(network, cdg, src_channel, escape = True, escape_paths = escape_paths)

# TODO this escape approach might not be working - investigate and fix if needed
def core_dijkstra(network, cdg, src_channel, escape = False, ignore = False, escape_paths = None):
    vlog('core dijkstra from channel {}'.format(src_channel))
    dist = {}
    used_channel = {}
    for node in network:
        dist[node] = inf
        used_channel[node] = None
    for channel in network.edges():
        dist[channel] = inf
        used_channel[channel] = None

    # TODO CHECK is this relation of channel/node distance correct?
    # dist[src_channel] = 0
    dist[src_channel[0]] = 0
    dist[src_channel[1]] = dist[src_channel] = cdg.nodes[src_channel]['weight']
    used_channel[src_channel[1]] = src_channel

    queue = [src_channel]

    while len(queue) > 0:
        dlog('queue {}'.format(queue))

        ##### TODO replace with min-queue pop
        cp = min(queue, key = lambda c: dist[c])
        queue.remove(cp)
        #####

        dlog('popped {}'.format(cp))

        for cq in cdg[cp]:
            if cdg.edges[cp, cq]['state'] == 'blocked':
                continue
            # route only using escape paths
            if escape and not (cp in escape_paths.edges() and cq in escape_paths.edges()):
                continue
            # consider only non-ignored out channels
            if ignore and cdg.nodes[cq]['ignored']:
                continue
            dlog('processing adjacent nonblocked {}'.format(cq))
            newdist = dist[cp] + cdg.nodes[cq]['weight']
            dlog('newdist {} distcp {} weightcq {} distcq {}'.format(newdist, dist[cp], cdg.nodes[cq]['weight'], dist[cq[1]]))
            if newdist < dist[cq[1]]:
                dlog('can update weight')
                if escape or is_cycle_free_with(cdg, (cp, cq)):
                    assert(cdg.edges[cp, cq]['state'] == 'used')
                    if used_channel[cq[1]] != None:
                        queue.remove(used_channel[cq[1]])
                    ##### TODO replace with add with weight
                    queue.append(cq)
                    #####

                    # TODO CHECK correct?
                    dist[cq] = dist[cq[1]] = newdist
                    used_channel[cq[1]] = cq
                    used_channel[cq] = cp
                else:
                    assert(cdg.edges[cp, cq]['state'] == 'blocked')
                    dlog('not cycle-free, blocked {}, {}'.format(cp, cq))

    return used_channel

def core_dijkstra_out_is_valid(used_channel, srcs, dst):
    for src in srcs:
        if src != dst and used_channel[opp(src)] == None:
            return False
    return True

def _tag_escape_paths_spanning_tree(spanning_tree, cdg):
    for (u, v) in spanning_tree.edges():
        def tag_adjacent_edges(cdg, tree, u, v):
            if tree.nodes[v]['terminal']:
                return
            for w in tree.adj[v]:
                if u == w:
                    continue
                assert(is_cycle_free_with(cdg, ((u, v), (v, w))))
                # above line basically tags (u,v), (v, w) as 'used' and confirms
                # it did not close any cycles i.e. cdg.edges[(u, v), (v,
                # w)]['state'] = 'used'
        tag_adjacent_edges(cdg, spanning_tree, u, v)
        tag_adjacent_edges(cdg, spanning_tree, v, u)

def tag_escape_paths_spanning_tree(topology, cdg):
    vlog('tagging escape paths using a spanning tree')
    # choose a switch as root
    root = random.choice([k for k, v in list(topology.nodes(data=True)) if not v['terminal']])
    spanning_tree = nx.minimum_spanning_tree(topology)

    _tag_escape_paths_spanning_tree(spanning_tree, cdg)

    return spanning_tree

def assign_initial_weights(network, cdg):
    vlog('assigning initial weights')
    # TODO is this appropriate for initial weights of channels?
    w = len(network.nodes()) ** 2
    for channel in cdg:
        cdg.nodes[channel]['weight'] = w

def used_channels_get_path(used_channel, v, u):
    # we walk the used channels backwards to get the path, however this should
    # still preserve deadlock freedom because a cycle-free graph with all edges
    # reversed is still cycle free (assuming for every channel (u,v), (v,u)
    # exists as well)
    #
    # TODO check if the the dependencies at the start/end of the paths are handled correctly
    if v == u:
        return []
    else:
        return [opp(used_channel[v])] + used_channels_get_path(used_channel, used_channel[v][0], u)

# Currently, no trackback and no smart spanning tree root choice
# TODO add weight updates
def nue_routing_single_layer(topology, nodes = None):
    # topology - undirected graph, edges are links
    ilog('nue_routing_single_layer')
    if nodes == None:
        nodes = [k for k, v in topology.nodes(data=True) if v['terminal']]
    # network - directed graph, edges are channels
    vlog('converting topology to network graph')
    network = nx.DiGraph(topology)
    cdg = complete_cdg(network)
    assign_initial_weights(network, cdg)
    cycle_detection_prep(cdg)
    tree = tag_escape_paths_spanning_tree(topology, cdg)

    paths = {}
    for u in nodes:
        # TODO channel or link?
        channel_use_counter = {channel: 0 for channel in network.edges()}

        used_channel = nue_dijkstra(network, cdg, tree, u)
        dlog('used_channel: {}'.format(used_channel))
        for v in nodes:
            dlog('paths for {}, {}'.format(v, u))
            if v == u:
                paths[v, u] = []
            else:
                paths[v, u] = used_channels_get_path(used_channel, v, u)
                for channel in paths[v, u]:
                    channel_use_counter[channel] += 1
        for channel in network.edges():
            cdg.nodes[opp(channel)]['weight'] += channel_use_counter[channel]
    return topology, network, cdg, paths, tree

def get_routing_tables(paths, network):
    forwarding_tables = {}
    # for switch in [node for node, data in network.nodes(data=True) if not data['terminal']]:
    #     forwarding_tables[switch] = {}
    for node in network.nodes:
        forwarding_tables[node] = {}
    for (src, dst), path in paths.items():
        # for every hop in the path
        if path == None:
            continue

        for (u, v) in path:
            if dst not in forwarding_tables[u]:
                forwarding_tables[u][dst] = v
            elif forwarding_tables[u][dst] != v:
                # not destination based
                print(forwarding_tables[u][dst])
                print(u, v)
                print((src, dst), path)
                return None
    return forwarding_tables


def reset_ignored_channels(cdg):
    for channel in cdg.nodes():
        cdg.nodes[channel]['ignored'] = False

def ignore_non_affected_channels(cdg, affected_nodes):
    for (u, v) in cdg.nodes():
        if u not in affected_nodes:
            cdg.nodes[u, v]['ignored'] = True

def get_entry_exit_route(route, affected_nodes):
    # find where the route first enters the subgraph
    entryi = 0
    while route[entryi][1] not in affected_nodes:
        entryi += 1
    entry_channel = route[entryi]

    # find where it last exits the subgraph
    exiti = len(route) - 1
    while route[exiti][0] not in affected_nodes:
        exiti -= 1
    exit_channel = route[exiti]

    assert(not (exit_channel[0] in affected_nodes and exit_channel[1] in affected_nodes))
    assert(not (entry_channel[0] in affected_nodes and entry_channel[1] in affected_nodes))
    return (entry_channel, exit_channel), (entryi, exiti)

'''
def escape_paths_graph_from_cdg(cdg):
    _cdg = cdg.copy()
    cdg = nx.subgraph_view(
        cdg, filter_edge = lambda u, v: _cdg.edges[u, v]['state'] == 'used').copy()
    return cdg
'''

# need to remove isolated terminal nodes
def remove_failed_spanning_tree(tree, failure):
    for v, u in failure['es']:
        tree.remove_edge(u, v)
    for node in failure['vs']:
        tree.remove_node(node)

def remove_failed_tncr(topology, network, cdg, routes, failure, tree = False):
    for v, u in failure['es']:
        network.remove_edge(v, u)
        network.remove_edge(u, v)
        topology.remove_edge(v, u)
        if tree and (v, u) in tree.edges():
            tree.remove_edge(v, u)

    for node in failure['vs']:
        network.remove_node(node)
        topology.remove_node(node)
        if tree:
            tree.remove_node(node)

    isolated = []
    # remove isolated terminal nodes
    for node in list(topology.nodes()):
        if topology.nodes[node]['terminal'] and len(topology.adj[node]) == 0:
            topology.remove_node(node)
            network.remove_node(node)
            if tree:
                tree.remove_node(node)
            isolated.append(node)

    # remove routes from/to isolated terminal nodes
    for (src, dst) in list(routes.keys()):
        if src in isolated or dst in isolated:
            routes.pop((src, dst))

    remove_failed_cdg(cdg, failure)

# does not remove isolated nodes
def remove_failed_network(network, failure):
    for v, u in failure['es']:
        network.remove_edge(v, u)
        network.remove_edge(u, v)
    for node in failure['vs']:
        network.remove_node(node)

def remove_failed_cdg(cdg, failure):
    dlog('removing failure from cdg {}'.format(failure))
    for channel in list(cdg.nodes()):
        u = channel[0]
        v = channel[1]
        c0 = (u, v)
        c1 = (v, u)
        if (c0 in failure['es'] or c1 in failure['es'] or
            v in failure['vs'] or u in failure['vs']):
            dlog('removing {}'.format(channel))
            cdg.remove_node(channel)

def reset_cdg_weights(cdg, affected_nodes):
    for u, v in cdg.nodes():
        if u in affected_nodes and v in affected_nodes:
            cdg.edges[edge]['weight'] = len(affected_nodes) ** 2

def reset_cdg_states(cdg, affected_nodes):
    for edge in cdg.edges():
        ((u, _v), (v, w)) = edge
        assert(_v == v)
        if v in affected_nodes:
            cdg.edges[edge]['state'] = 'unused'

def find_affected_routes(routes, affected_nodes):
    def _route_affected(route, affected_nodes):
        for (u, v) in route:
            if u in affected_nodes or v in affected_nodes:
                return True
        return False
    affected_routes = {vs: route for vs, route in routes.items() if _route_affected(route, affected_nodes)}
    return affected_routes

# the nodes which must be included in the recalculation area
#
# at this point this does not consider the routes, it assumes that all failed
# connections were actually used TODO consider if it is worth it
def nodes_in_minimum_recalculation_area(topology, routes, failure):
    ra = []
    # TODO not correct as we need to consider already failed components as
    # well, works for singular edge or node failures
    for v in failure['vs']:
        for u in topology.adj[v]:
            if not topology.nodes[u]['terminal']:
                ra.append(u)
    for u, v in failure['es']:
        if not topology.nodes[u]['terminal']:
            ra.append(u)
        if not topology.nodes[v]['terminal']:
            ra.append(v)

    return ra

# Single layer implementation
class LocalNue:
    def __init__(self, topology, network, cdg, routes,
                 target_size = 0,
                 spanning_tree = None,
                 ra_expansion_type = 'topology'):
        self.topology = topology.copy()
        self.network = network.copy()
        self.cdg = cdg.copy()
        self.routes = copy.deepcopy(routes)

        # escape path related bookkeeping
        self.times_added = {}

        # affected area target size
        self.target_size = target_size

        # how to expand the ra
        self.ra_expansion_type = ra_expansion_type

        # spanning tree for escape path calculation
        if spanning_tree:
            self.spanning_tree = spanning_tree.copy()

        self.global_used_channels = {}

        self.eval_time = OrderedDict()

    def is_cycle_free_with_path(self, path):
        for i, edge in enumerate(zip(path, path[1:])):
            res = is_cycle_free_with_push(self.cdg, edge)
            if res == 'already_existed':
                continue
            elif res == 'blocked':
                self.revert_path(path[: i + 2])
                return False
            else:
                assert(res == 'added')
        return True

    # removes path from cdg and restores states and cycle detection bookkeeping
    def revert_path(self, path):
        for edge in reversed(list(zip(path, path[1:]))):
            is_cycle_free_with_pop(self.cdg, edge)

    def _search_dl_free_paths(self, paths, curr):
        if len(paths) == 0:
            return True
        for path in paths[-1]:
            # adds the path to the cdg and checks if the result is cycle-free
            dlog('adding path {}: '.format(path), end='')
            if self.is_cycle_free_with_path(path):
                dlog('cycle free')
                if self._search_dl_free_paths(paths[:-1], [path] + curr):
                    return True
                # remove the path from the cdg
                self.revert_path(path)
                dlog('reverting {}'.format(path))
            else:
                dlog('blocked')

        else:
            return False

    def search_dl_free_paths(self, paths):
        return self._search_dl_free_paths(paths, [])

    def raw_tag_paths_used(self, paths):
        for path in paths:
            for c1, c2 in zip(path, path[1:]):
                self.cdg.edges[c1, c2]['state'] = 'used'

    def are_paths_dl_free(self, paths):
        reset_cdg_states(self.cdg, self.affected_nodes)
        self.raw_tag_paths_used(paths)
        res = my_is_cycle_free(self.cdg, paths[0][0])
        return res

    def _get_all_paths_between(self, src, dst, path):
        self.ap_visited[src[0]] = self.ap_visited[src[1]] = True
        path.append(src)

        if src == dst:
            self.all_paths.append(copy.copy(path))
        else:
            for channel in self.cdg.adj[src]:
                if self.ap_visited[channel[1]]:
                    continue
                if self.cdg.nodes[channel]['ignored']:
                    continue
                self._get_all_paths_between(channel, dst, path)
        path.pop()
        self.ap_visited[src[1]] = False

    def get_all_paths_between(self, src, dst):
        dlog('paths between {} and {}'.format(src, dst))
        self.all_paths = []
        self.ap_visited = {}
        for node in self.topology:
            self.ap_visited[node] = False
        self._get_all_paths_between(src, dst, [])
        dlog(self.all_paths)
        return self.all_paths

    # here H is a set
    def tag_local_nue_escape_paths(self, H):
        ilog('escape paths: finding all possible paths')
        paths = {}
        for pair in H:
            paths[pair] = self.get_all_paths_between(pair[0], pair[1])
            # TODO should we shuffle?
            # random.shuffle(paths[pair])

        ilog('escape paths: searching for a dl free combination')
        paths_v = list(paths.values())
        found_dl_free_paths = self.search_dl_free_paths(paths_v)
        return found_dl_free_paths

    def get_affected_area_subgraph(self, failure):
        g = self.topology.copy()
        # affected area
        # TODO not correct as we need to consider already failed components as
        # well, works for singular edge or node failures
        for v in failure['vs']:
            g.remove_node(v)
        for u, v in failure['es']:
            g.remove_edge(u, v)

        aa = nodes_in_minimum_recalculation_area(self.topology, self.routes, failure)

        dlog('failure {}, aa {}, g {}'.format(failure, aa, g.nodes()))

        to_visit = copy.copy(aa)
        aa = g.subgraph(aa).copy()

        # ensure aa is connected
        # TODO len(nodes) > target size correct?
        while not nx.is_connected(aa) and len(aa.nodes()) > self.target_size and not len(to_visit) == 0:
            u = to_visit.pop(0)
            for v in g.neighbors(u):
                if v in aa.nodes() or self.topology.nodes[v]['terminal']:
                    continue
                aa.add_node(v)
                to_visit.append(v)
                for w in g.neighbors(v):
                    if w in aa.nodes():
                        aa.add_edge(v, w)

        dlog('res {}, {}'.format(aa.nodes(), aa.edges()))

        return aa

    def find_H_pairs(self, ra):
        # find our src/dst pairs we need to route between

        affected_routes = find_affected_routes(self.routes, ra)
        self.affected_routes = affected_routes

        H = {}
        Hi = {}
        Hd = {}
        for (src, dst), route in affected_routes.items():
            h, hi = get_entry_exit_route(route, ra)
            H[(src, dst)] = h
            Hi[(src, dst)] = hi

            if h[1] not in Hd:
                Hd[h[1]] = []
            Hd[h[1]].append(h[0])

        dlog('H {}'.format(H))

        self.H = H
        self.Hi = Hi
        self.Hd = Hd

    def preprocess(self, failure):
        affected_subgraph = self.get_affected_area_subgraph(failure)
        affected_nodes = list(affected_subgraph.nodes())
        self.affected_nodes = affected_nodes

        vlog('affected_nodes {}'.format(affected_nodes))

        # remove failed components
        remove_failed_tncr(self.topology, self.network, self.cdg, self.routes, failure)
        # TODO do we want to have the isolated notes?

        self.find_H_pairs(self.affected_nodes)

        # set which channels dijkstra should ignore
        reset_ignored_channels(self.cdg)
        ignore_non_affected_channels(self.cdg, affected_nodes)

        reset_cdg_states(self.cdg, affected_nodes)

        # this resets all of the cycle detection bookkeeping, needed because
        # removing nodes and altering states invalidates it
        cycle_detection_prep(self.cdg)

        H = self.H
        Hi = self.Hi
        # convert to a set for now
        Hset = set(H.values())

        self.found_escape_paths = self.tag_local_nue_escape_paths(Hset)
        if self.found_escape_paths:
            ilog('local_nue: found escape paths')
        else:
            ilog('local_nue: escape paths do not exist')

        self.local_routes = self.local_route(Hset)


    def get_route(self, pair):
        def try_route(route_before, route_after, local_route, indices, in_out_channels):
            if local_route == None:
                return True, None, None

            local_route_for_db = [in_out_channels[0]] + local_route + [in_out_channels[1]]

            is_dst_based, src = is_destination_based(local_route_for_db, pair)
            if is_dst_based:
                route = self.repair_route(route_before, route_after, local_route, indices)
                return True, route, None
            else:
                return False, None, src

        def is_destination_based(local_route, pair):
            (src, dst) = pair
            for (u, v) in local_route:
                if dst not in self.forwarding_tables[u]:
                    self.forwarding_tables[u][dst] = (v, src)
                elif self.forwarding_tables[u][dst][0] != v:
                    return False, self.forwarding_tables[u][dst][1]
            return True, None

        success, route, src = try_route(self.routes[pair], self.routes[pair],
                                        self.local_routes[self.H[pair]],
                                        self.Hi[pair],
                                        self.H[pair])
        if success:
            return route

        # pair of the other route (the one we are trying to merge with in the RA)
        pair2 = (src, pair[1])

        if self.local_used_channel[self.H[pair2][1]][opp(self.H[pair][0])] == None:
            return None

        success, route, _ = try_route(self.routes[pair], self.routes[pair2],
                                      used_channels_get_path(
                                          self.local_used_channel[self.H[pair2][1]],
                                          self.H[pair][0][1],
                                          self.H[pair2][1][0]),
                                      (self.Hi[pair][0], self.Hi[pair2][1]),
                                      (self.H[pair][0], self.H[pair2][1]))
        if success:
            return route

        return None

    def repair_route(self, route_before, route_after, local_route, indices):
        if local_route == None:
            self.eval_global_routes += 1
            return None
        self.eval_local_routes += 1
        (entryi, exiti) = indices
        return route_before[: entryi + 1] + local_route + route_after[exiti :]

    def repair_routes(self):
        self.reset_route_stats()
        self.forwarding_tables = {node: {} for node in self.network.nodes}
        self.new_routes = {}

        for pair in self.H.keys():
            route = self.get_route(pair)
            self.new_routes[pair] = route
            #assert(get_routing_tables(self.new_routes, self.network) != None)

    def repair_unsuccessful_routes(self):
        self.eval_global_dijkstra_runs = 0
        self.eval_global_dijkstra_routes = 0
        self.new_escape_routes = {}

        for pair, route in list(self.new_routes.items()):
            if route != None:
                continue
            # reroute using escape paths on the whole graph because local
            # rerouting failed
            (src, dst) = pair
            dijkstra_src_channel = (dst, list(self.topology.adj[dst])[0])
            if dst not in self.global_used_channels:
                self.eval_global_dijkstra_runs += 1
                self.global_used_channels[dst] = core_dijkstra(self.network, self.cdg, dijkstra_src_channel,
                                                               escape = True,
                                                               escape_paths = self.spanning_tree)
            route = used_channels_get_path(self.global_used_channels[dst], src, dst)
            self.new_escape_routes[pair] = route
            self.eval_global_dijkstra_routes += 1

    def reset_route_stats(self):
        self.eval_local_routes = 0
        self.eval_global_routes = 0

    # join two strongly connected tree components without closing cycles
    def join_two_trees(self, t1, t2, ra = None):
        def tag_channel_from_to_tree(cdg, from_tree, to_tree, u, v):
            assert(v in to_tree.nodes())
            edges_added = []
            for w in to_tree.adj[v]:
                assert(u != w)
                edge = (u, v), (v, w)
                edge_state = cdg.edges[edge]['state']

                res = is_cycle_free_with_push(self.cdg, edge)
                if res == 'added':
                    edges_added.append(edge)
                elif res == 'already_existed':
                    continue
                elif res == 'blocked':
                    edges_added.append(edge)
                    return False, edges_added
                else:
                    assert(False)
            for w in from_tree.adj[u]:
                assert(u != w)
                edge = (w, u), (u, v)
                edge_state = cdg.edges[edge]['state']

                res = is_cycle_free_with_push(self.cdg, edge)
                if res == 'added':
                    edges_added.append(edge)
                elif res == 'already_existed':
                    continue
                elif res == 'blocked':
                    edges_added.append(edge)
                    return False, edges_added
                else:
                    assert(False)
            return True, edges_added

        def revert_tag(cdg, edges_added):
            for edge in edges_added:
                is_cycle_free_with_pop(cdg, edge)

        def _tag(cdg, t1, t2, u, v):

            res, edges_added = tag_channel_from_to_tree(cdg, t1, t2, u, v)
            if res:
                return True
            else:
                revert_tag(cdg, edges_added)
                return False


        channels = list(self.network.edges())
        # has to be the channels, not the links, because we consider only the
        # single direction

        # TODO is it actually a good idea to shuffle the channels? (probably
        # best to choose the most 'central' nodes first
        random.shuffle(channels)
        for u, v in channels:
            if u in t1.nodes() and v in t2.nodes():
                if ra and not(u in ra and v in ra):
                    continue
                res1, edges_added1 = tag_channel_from_to_tree(self.cdg, t1, t2, u, v)
                if res1:
                    res2, edges_added2 = tag_channel_from_to_tree(self.cdg, t2, t1, v, u)
                    if res2:
                        self.spanning_tree.add_edge(u, v)
                        return True
                    else:
                        revert_tag(self.cdg, edges_added2)
                        revert_tag(self.cdg, edges_added1)
                else:
                    revert_tag(self.cdg, edges_added1)

        return False

    def find_broken_st_components(self):
        components = nx.connected_components(self.spanning_tree)
        trees = [self.spanning_tree.subgraph(component) for component in components]
        for i, tree in enumerate(trees):
            for node in tree.nodes():
                self.topology.nodes[node]['tree_id'] = i
        self.broken_st_components = trees
        self.eval_number_of_spanning_tree_components = len(trees)

    def fix_st_escape_paths(self, ra = None):
        vlog('fixing escape paths, trees: {}'.format(len(self.broken_st_components)))

        trees = self.broken_st_components

        # TODO figure out a better way to reset cycle search state
        # TODO 2020-12-14 is this actually needed???
        for tree in trees:
            _tag_escape_paths_spanning_tree(tree, self.cdg)

        current_tree = trees.pop()
        while(len(trees) > 0):
            to_add = trees.pop()
            if not self.join_two_trees(current_tree, to_add, ra):
                return False
        return True

    def expand_single_tree_ra(self, tree, ra):
        def _tree_bfs(tree, src, dsts):
            dsts = set(dsts)
            prev = {src: None}
            queue = [src]

            dlog('tree', tree.edges())
            while len(dsts) > 0: # len(queue) > 0  should always be true as long as all the dsts are reachable
                dlog('queue', queue)
                dlog('prev', prev)
                u = queue.pop(0)
                for v in tree.neighbors(u):
                    if v in prev:
                        continue
                    else:
                        prev[v] = u
                        queue.append(v)
                        dsts.discard(v)
            return prev

        new_ra = set()
        ra_nodes_in_tree = {u for u in ra if u in tree.nodes()}
        src = ra_nodes_in_tree.pop()
        dsts = ra_nodes_in_tree
        prev = _tree_bfs(tree, src, dsts)
        for dst in dsts:
            p = prev[dst]
            while p != None:
                new_ra.add(p)
                p = prev[p]
        return new_ra


    def expand_min_ra_after_broken_st(self, ra_nodes):
        trees = self.broken_st_components
        new_ra_nodes = set(ra_nodes)

        # ensure nodes in the RA will be reachable using escape paths
        for tree in trees:
            tmp = self.expand_single_tree_ra(tree, ra_nodes)
            new_ra_nodes.update(tmp)
        return new_ra_nodes

    def reroute_alt_st_preprocess(self, failure):
        ilog('local_nue rerouting single layer alt st')
        assert(self.spanning_tree != None)

        # remove failed components
        remove_failed_tncr(self.topology, self.network, self.cdg, self.routes, failure, self.spanning_tree)
        # remove_failed_spanning_tree(self.spanning_tree, failure)

        # need to save which edges are used and readd after resetting cycle
        # detection and state information because we cannot reset them only in
        # the recalculation area yet

        # TODO check in the preprocess function, we are doing it another way,
        # does it not work?
        used_edges = [(u, v) for u, v, data in self.cdg.edges(data = True) if data['state'] == 'used']

        cycle_detection_prep(self.cdg)

        reset_cdg_states(self.cdg, list(self.topology.nodes()))
        for edge in used_edges:
            # cycle-detection bookkeeping + self.cdg.edges[edge]['state'] = 'used'
            assert(is_cycle_free_with(self.cdg, edge))

        # fix broken escape paths
        self.find_broken_st_components()
        success = self.fix_st_escape_paths()

        self.eval_fix_st_escape_paths_success = success

    # does not make sure the RA is connected
    def expand_recalculation_area_by(self, ra, g):
        queue = list(ra)
        #ra = copy.copy(ra)

        while len(queue) > 0 and len(ra) < self.target_size:
            u = queue.pop(0)
            for v in g.neighbors(u):
                if v in ra or self.topology.nodes[v]['terminal']:
                    continue
                ra.add(v)
                queue.append(v)
        if len(queue) == 0:
            ilog('!!! ra expansion stopped because no nodes were left to process')

    def reroute_alt_st_v2_preprocess(self, failure):
        ilog('local_nue rerouting single layer alt st v2')

        min_ra = nodes_in_minimum_recalculation_area(self.topology, self.routes, failure)
        vlog('min_ra {}'.format(min_ra))

        remove_failed_tncr(self.topology, self.network, self.cdg, self.routes, failure, self.spanning_tree)

        self.find_broken_st_components()
        vlog('min_ra', min_ra)
        ra = self.expand_min_ra_after_broken_st(min_ra)
        vlog('expanded ra', ra)

        # TODO have to do the RA expansion while preserving escape paths
        # reachibility within it TODO perhaps by expanding the part of the
        # trees in the affected area (good to keep nodes on the edge of
        # expansion maybe)

        self.expand_recalculation_area_by(ra, self.spanning_tree)

        vlog('expanded ra', ra)

        self.reset_cdg_and_cycle_detection_states(ra)

        # TODO we are relying on the fix_st_escape_paths function to re-add the
        # tree edges, we have to add them here

        # TODO make a version which accepts list of edges to use (and give it
        # the edges in the recalculation area
        success = self.fix_st_escape_paths(ra)
        self.eval_fix_st_escape_paths_success = success

        self.affected_nodes = ra

        self.find_H_pairs(ra)

    def get_links_between_different_trees(self):
        return [(u, v) for (u, v) in self.topology.edges()
                if self.topology.nodes[u]['tree_id'] != self.topology.nodes[v]['tree_id']]

    def get_tree_edges(self):
        min_ra = set()
        links = self.get_links_between_different_trees()
        for (u, v) in links:
            min_ra.add(u)
            min_ra.add(v)
        return min_ra

    def reset_cdg_and_cycle_detection_states(self, ra):
        # need to save which edges are used and re-add after resetting cycle
        # detection and state information because we cannot reset them only in
        # the recalculation area yet

        # TODO check in the preprocess function, we are doing it another way,
        # does it not work?

        reset_cdg_states(self.cdg, ra)

        used_edges = [(u, v) for u, v, data in self.cdg.edges(data = True)
                      if data['state'] == 'used']

        reset_cdg_states(self.cdg, list(self.topology.nodes()))
        cycle_detection_prep(self.cdg)
        for edge in used_edges:
            # cycle-detection bookkeeping + self.cdg.edges[edge]['state'] = 'used'
            assert(is_cycle_free_with(self.cdg, edge))

    def reset_cdg_and_cycle_detection_states_fast(self, ra):
        # wipes all cycle-detection bookkeeping
        reset_cdg_states(self.cdg, ra)
        cycle_detection_prep(self.cdg)

        edge_channels = [(u, v) for u, v in self.network.edges()
                         if (u in ra and v not in ra) or (v in ra and u not in ra)]

        # TODO is the copy needed?
        cdg = self.cdg.copy()
        fcdg = nx.subgraph_view(self.cdg, filter_edge = lambda u, v: cdg.edges[u, v]['state'] == 'used')

        dlog('ra', ra)
        dlog('ec', edge_channels)
        dlog('ed', fcdg.edges())
        dlog('nn', fcdg.nodes())

        for i, component in enumerate(nx.weakly_connected_components(fcdg), 1):
            component = list(component)
            dlog(component)
            for channel in edge_channels:
                if channel in component:
                    self.cdg.nodes[channel]['omega'] = i
            self.cdg.graph['cfgroupid'] += 1


    def tag_st_escape_paths_in_ra(self):
        # difference to _tag_escape_paths_spanning_tree is that this function
        # only changes edge states in the RA
        for (u, v) in self.spanning_tree.edges():
            def tag_adjacent_edges(cdg, tree, u, v):
                if tree.nodes[v]['terminal']:
                    return
                for w in tree.adj[v]:
                    if u == w:
                        continue
                    edge = ((u, v), (v, w))
                    if cdg.edges[edge]['state'] != 'used':
                        assert(is_cycle_free_with(cdg, ((u, v), (v, w))))
                    # above line basically tags (u,v), (v, w) as 'used' and confirms
                    # it did not close any cycles i.e. cdg.edges[(u, v), (v,
                    # w)]['state'] = 'used'
            tag_adjacent_edges(self.cdg, self.spanning_tree, u, v)
            tag_adjacent_edges(self.cdg, self.spanning_tree, v, u)

    def add_connecting_links_broken_st(self):
        tree_num = len(self.broken_st_components)
        trees = list(range(1, tree_num))
        tree_id_map = {i: i for i in range(tree_num)}
        links = 0
        for (u, v) in self.network.edges():
            u_id = self.topology.nodes[u]['tree_id']
            v_id = self.topology.nodes[v]['tree_id']
            if tree_id_map[u_id] != tree_id_map[v_id]:
                self.spanning_tree.add_edge(u, v)
                change_from = tree_id_map[u_id]
                change_to = tree_id_map[v_id]
                for i in tree_id_map:
                    if tree_id_map[i] == change_from:
                        tree_id_map[i] = change_to
                links += 1
                dlog(tree_id_map, u_id, v_id, links)
        if links != tree_num - 1:
            raise Exception('No connecting link found')

    def call_and_add_time(self, fun, *args):
        start_time = time.process_time()
        res = fun(*args)
        end_time = time.process_time()
        self.eval_time[fun.__name__] = end_time - start_time
        return res

    def reroute_alt_st_v3_preprocess(self, failure):

        ra = set(self.call_and_add_time(nodes_in_minimum_recalculation_area, self.topology, self.routes, failure))

        self.call_and_add_time(remove_failed_tncr, self.topology, self.network, self.cdg, self.routes, failure, self.spanning_tree)

        self.call_and_add_time(self.find_broken_st_components, )

        self.call_and_add_time(ra.update, self.get_tree_edges())

        if self.ra_expansion_type == 'tree':
            ra = self.call_and_add_time(self.expand_min_ra_after_broken_st, ra)
            self.call_and_add_time(self.expand_recalculation_area_by, ra, self.spanning_tree)
        elif self.ra_expansion_type == 'topology':
            self.call_and_add_time(self.expand_recalculation_area_by, ra, self.topology)
        else:
            assert(False)

        self.call_and_add_time(self.reset_cdg_and_cycle_detection_states_fast, ra)

        self.call_and_add_time(self.add_connecting_links_broken_st, )

        self.call_and_add_time(self.tag_st_escape_paths_in_ra, )

        assert(len(list(nx.connected_components(self.spanning_tree))) == 1)

        self.affected_nodes = ra

        self.call_and_add_time(self.find_H_pairs, ra)

    def combine_routes(self):
        for ends in self.new_routes:
            if self.new_routes[ends] == None:
                self.routes[ends] = self.new_escape_routes[ends]
            else:
                self.routes[ends] = self.new_routes[ends]

    # failure is a set of failed nodes failure['vs'] and failed undirected
    # links failure['es']
    def reroute(self, failure = None, preprocess = True):
        ilog('local_nue rerouting single layer')
        # H is the multiset of all src/dst pairs keyed by the src/dst pair of
        # terminal nodes they correspond to, it could be used for
        # load-balancing at the cost of greater computational requirements
        if preprocess and failure != None:
            self.preprocess(failure)

        # set which channels dijkstra should ignore
        self.call_and_add_time(reset_ignored_channels, self.cdg)
        self.call_and_add_time(ignore_non_affected_channels, self.cdg, self.affected_nodes)

        self.call_and_add_time(assign_initial_weights, self.network, self.cdg)

        self.local_routes = self.call_and_add_time(self.local_route, self.Hd)

        # list of H keys is the list of affected routes (src/dst terminal node
        # pairs)
        self.call_and_add_time(self.repair_routes, )

        self.call_and_add_time(self.repair_unsuccessful_routes, )

        self.call_and_add_time(self.combine_routes, )

    # TODO weight updates
    def local_route(self, Hd):
        self.eval_local_dijkstra_runs = 0
        self.eval_local_dijkstra_routes = 0
        self.eval_local_escape_dijkstra_routes = 0

        paths = {}
        #paths_escape = {}
        used_channel = {}
        #used_channel_escape = {}
        for dst, srcs in Hd.items():
            self.eval_local_dijkstra_runs += 1
            used_channel[dst] = core_dijkstra(self.network, self.cdg, opp(dst), ignore = True)
            #used_channel_escape[dst] = core_dijkstra(self.network, self.cdg, opp(dst),
            #                                         ignore = True, escape = True, escape_paths = self.spanning_tree)

            for src, route_num in statistics.Counter(srcs).items():
                # TODO need to track which paths come from which dijkstra run
                # for assigning different addresses to them (for ensuring
                # destination-basedness)
                if used_channel[dst][opp(src)] != None:
                    self.eval_local_dijkstra_routes += 1
                    paths[src, dst] = used_channels_get_path(used_channel[dst], src[1], dst[0])
                #elif used_channel_escape[dst][opp(src)] != None:
                #    self.eval_local_escape_dijkstra_routes += 1
                #    paths_escape[src, dst] = path = used_channels_get_path(used_channel_escape[dst], src[1], dst[0])
                else:
                    paths[src, dst] = None

                # update weights
                if paths[src, dst] != None:
                    for channel in paths[src, dst]:
                        self.cdg.nodes[opp(channel)]['weight'] += route_num

        self.local_used_channel = used_channel
        #self.local_used_channel_escape = used_channel_escape

        return paths

    def get_eval_package(self):

        self.eval_switch_num = len([n for n in self.topology.nodes() if not self.topology.nodes[n]['terminal']])
        self.eval_terminal_num = len([n for n in self.topology.nodes() if self.topology.nodes[n]['terminal']])
        self.eval_ra_size = len(self.affected_nodes)
        self.eval_affected_route_num = len(self.affected_routes)

        package = {}
        for attrib in dir(self):
            if attrib.startswith('eval'):
                package[attrib] = self.__getattribute__(attrib)
        return package
