import networkx as nx
import statistics

def routing_cdg(routing):
    cdg = nx.DiGraph()
    for route in routing.values():
        if route == None:
            continue
        i = 0
        while i + 1 < len(route):
            cdg.add_edge(route[i], route[i + 1])
            i += 1
    return cdg

def routing_is_deadlock_free(routing):
    cdg = routing_cdg(routing)
    try:
        cycle = nx.find_cycle(cdg)
    except nx.NetworkXNoCycle:
        return True
    return False

def edge_loads(routing, network):
    loads = {channel: 0 for channel in network.edges()
             if not network.nodes[channel[0]]['terminal'] and not network.nodes[channel[1]]['terminal']}
    for ends, route in routing.items():
        for edge in route:
            if edge in loads:
                loads[edge] += 1
    return loads

def edge_forwarding_indices(routing, network):
    return list(edge_loads(routing, network).values())

