import networkx as nx
from functools import reduce

def remove_failed_subgraph(g, failure):
    sg = nx.subgraph_view(
        g,
        filter_node = lambda v: v not in failure['vs'],
        filter_edge = lambda v, u: ((v, u) not in failure['es'] and
                                    (u, v) not in failure['es']))
    # TODO need to act if the graph is not connected
    assert(nx.is_connected(sg))
    return sg

def remove_failed(g, failure):
    for v, u in failure['es']:
        g.remove_edge(v, u)
    for node in failure['vs']:
        g.remove_node(node)
    # TODO need to act if the graph is not connected
    assert(nx.is_connected(g))

def route_and_reroute(g, failure, routing_fun):
    proutes = routing_fun(g)
    return reroute(g, proutes, failure, routing_fun)

def repair_route(route, subgraph_routes, subgraph):
    route_from = route[0][0]
    route_to = route[-1][1]

    # if start and end of the route are in the subgraph just get the subgraph route
    if route_from in subgraph.nodes and route_to in subgraph.nodes:
        return subgraph_routes[route_from, route_to]

    # find where the route first enters the subgraph
    enteri = 0
    while route[enteri][0] not in subgraph.nodes:
        enteri += 1
    enter_node = route[enteri][0]

    # find where it last exits the subgraph
    exiti = len(route)
    while route[exiti - 1][1] not in subgraph.nodes:
        exiti -= 1
    exit_node = route[exiti - 1][1]

    # repair the part in the subgraph
    return route[: enteri] + subgraph_routes[enter_node, exit_node] + route[exiti :]

# Topology graph, existing (previous) routing, failure
def reroute(g, proutes, failure, routing_fun):
    subgraph, subgraph_with_removed = get_affected_subgraph(g, failure)
    affected_routes = find_affected_routes(g, proutes, subgraph)
    subgraph_routes = routing_fun(subgraph_with_removed)

    nroutes = proutes.copy()
    for (v, u), route in affected_routes.items():
        if v in failure['vs'] or u in failure['vs']:
            del nroutes[v, u]
        else:
            nroutes[v, u] = repair_route(route, subgraph_routes, subgraph)

    return nroutes

def find_affected_routes_f(g, routes, failure):
    subgraph, _ = get_affected_subgraph(g, failure)
    return find_affected_routes(g, routes, subgraph)

def find_affected_routes(g, routes, affected_subgraph):
    def _route_affected(affected_subgraph, route):
        for edge in route:
            if affected_subgraph.has_edge(edge[0], edge[1]):
                return True
        return False
    affected_routes = {vs: route for vs, route in routes.items() if _route_affected(affected_subgraph, route)}
    return affected_routes

def partitions(g):
    partset = {v for k, v in nx.get_node_attributes(g, 'partition').items()}
    partitions = {part: [v for k, v in nx.get_node_attributes(g, 'partition').items() if v == part]
                  for part in partset}
    return partitions

def get_affected_subgraph(g, f):
    sg = g.subgraph(get_nodes_in_affected_partitions(g, f))
    sgr = remove_failed_subgraph(sg, f)

    if not nx.is_connected(sgr):
        assert(False)
        # have to make it connected
    return sg, sgr

def get_nodes_in_affected_partitions(g, f):
    partitions = get_affected_partitions(g, f)
    return [k for k, v in nx.get_node_attributes(g, 'partition').items() if v in partitions]

def get_affected_partitions(g, f):
    return (reduce(lambda x, y: x|y, [get_affected_partitions_v(g, v) for v in f['vs']], set()) |
            reduce(lambda x, y: x|y, [get_affected_partitions_e(g, e) for e in f['es']], set()))

def get_affected_partitions_v(g, v):
    return {nx.get_node_attributes(g, 'partition')[v] for v in [v] + list(g.adj[v]) if g.nodes[v]['partition'] >= 0}

def get_affected_partitions_e(g, e):
    return {nx.get_node_attributes(g, 'partition')[v] for v in [e[0], e[1]] if g.nodes[v]['partition'] >= 0}
