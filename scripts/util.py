import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

___NONE_, ___INFO_, ___VERBOSE_, ___DEBUG_ = range(4)

log_mode = ___INFO_

def dlog(*args, **kwargs):
    if log_mode >= ___DEBUG_:
        print(*args, **kwargs)

def ilog(*args, **kwargs):
    if log_mode >= ___INFO_:
        print(*args, **kwargs)

def vlog(*args, **kwargs):
    if log_mode >= ___VERBOSE_:
        print(*args, **kwargs)
